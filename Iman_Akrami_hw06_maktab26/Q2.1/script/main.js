let numberGetterElem = document.getElementById("number-getter-button");
let numberOfPlayers;
let playerNameArray = [];
let playerInfoArrayOfObjects = [];
let gameCellHTMLArray;
let turn = 0;
let infotext;
//============================game table
//Creating race cells using javascript element generation. and assiging ids
//to each element corrsponding to the future array.
for (let index = 0; index < 300; index++) {
  let div = document.createElement("div");
  div.innerHTML = "&#10687";
  div.id = index;
  document.getElementById("game-frame").appendChild(div);
  document.getElementById("game-frame").classList.add("game-frame");
  document.getElementById("play-frame").style.display = "none";
  document.getElementById("winning-screen").style.display = "none";
  document.getElementById("name-getter-title").style.display = "none";
  gameCellHTMLArray = document.getElementById("game-frame").children;
}
//************************************************************************************ */
//=============================player mover function
//************************************************************************************ */
function playerMover(currentPlayerObject) {
  gameCellHTMLArray[currentPlayerObject["location"]].innerHTML = "&#10687";
  gameCellHTMLArray[currentPlayerObject["location"]].style.backgroundColor =
    "white";

  diceRoll = Math.floor(Math.random() * 10) + 1;
  currentPlayerObject["location"] += diceRoll;
  if (playerInfoArrayOfObjects[turn]["location"] > 299) {
    document.getElementById("game-frame").innerHTML =
      '<h1 class="winning-screen">' +
      playerInfoArrayOfObjects[turn]["name"] +
      " HAS WON! YAAAAAAAY!</h1>";
  }
  gameCellHTMLArray[currentPlayerObject["location"]].innerHTML =
    currentPlayerObject["name"];
  gameCellHTMLArray[currentPlayerObject["location"]].style.backgroundColor =
    "black";

  for (let index = 0; index < playerInfoArrayOfObjects.length; index++) {
    if (
      currentPlayerObject["location"] ==
        playerInfoArrayOfObjects[index]["location"] &&
      playerInfoArrayOfObjects[index]["location"] &&
      currentPlayerObject != playerInfoArrayOfObjects[index]
    ) {
      infotext =
        currentPlayerObject["name"] +
        " hit " +
        playerInfoArrayOfObjects[index]["name"] +
        "! go back to the start!";
      playerInfoArrayOfObjects[index]["location"] = 0;
    } else {
      infotext =
        currentPlayerObject["name"] +
        " rolled " +
        diceRoll +
        "and moved forward!";
    }
    document.getElementById("play-info-box").innerHTML = infotext;
  }
}

//************************************************************************************ */
//=============================running the game
//************************************************************************************ */

numberGetterElem.addEventListener("click", function() {
  document.getElementById("number-getter").style.display = "none";

  numberOfPlayers = document.getElementById("number-getter-num").value;

  ///**************************creating the input name field***************************** */
  document.getElementById("name-getter").style.display = "block";

  for (let index = 0; index < numberOfPlayers; index++) {
    let nameField = document.createElement("input");
    nameField.type = "text";
    nameField.id = "name" + index;

    document.getElementById("name-getter").appendChild(nameField);
  }
  ///**************************creating the input name field button***************************** */

  let nameButton = document.createElement("input");
  nameButton.type = "button";
  nameButton.value = "Start";
  nameButton.id = "name-getter-button";
  document.getElementById("name-getter").appendChild(nameButton);
  document.getElementById("name-getter").classList.add("name-getter");
  document.getElementById("name-getter-button").classList.add("btn");
  document.getElementById("name-getter-title").style.display = "block";
  document
    .getElementById("name-getter-title")
    .classList.add("name-getter-title");
  let nameGetterElem = document.getElementById("name-getter-button");
  nameGetterElem.addEventListener("click", function() {
    let namesHTMLArray = document.getElementById("name-getter").children;
    //=============================== Making array out of given names
    for (let index = 0; index < numberOfPlayers; index++) {
      playerNameArray[index] = namesHTMLArray[index].value;
    }
    shuffle(playerNameArray);

    for (let index = 0; index < numberOfPlayers; index++) {
      playerInfoArrayOfObjects[index] = {};
    }

    for (let player = 0; player < playerInfoArrayOfObjects.length; player++) {
      playerInfoArrayOfObjects[player]["name"] = playerNameArray[player];
      playerInfoArrayOfObjects[player]["location"] = 0;
    }

    document.getElementById("name-getter").style.display = "none";
    document.getElementById("name-getter-title").style.display = "none";

    document.getElementById("play-frame").style.display = "block";
    document.getElementById("play-frame").classList.add("play-frame");
    document.getElementById("play-info-box").classList.add("play-info-box");

    document.getElementById("player-order-text").innerHTML = playerNameArray;
    document.getElementById("play-frame-button").value =
      playerNameArray[turn] + " , start!";
    document.getElementById("play-frame-button").classList.add("btn");

    ///==================================================play

    document
      .getElementById("play-frame-button")
      .addEventListener("click", function() {
        playerMover(playerInfoArrayOfObjects[turn]);

        turn++;
        if (turn > numberOfPlayers - 1) {
          turn = 0;
        }

        document.getElementById("play-frame-button").value =
          playerNameArray[turn] + " , it's your turn!";
        document.getElementById("play-info-box").classList.remove("shake");
        void document.getElementById("play-info-box").offsetWidth;
        document.getElementById("play-info-box").classList.add("shake");

        console.log(turn);
      });
  });
});
//==================================Array shuffler
function shuffle(Arr) {
  for (let i = Arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [Arr[i], Arr[j]] = [Arr[j], Arr[i]];
  }
  return Arr;
}
//=====================================Player Object creator
function playerOBJcreator(index, name, position) {
  this.name = name;
  this.index = index;
  this.position = position;
}
//=============================== Making array out of given names
