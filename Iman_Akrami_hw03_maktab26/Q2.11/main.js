console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.11 starts from here:
// ======================================

// The test object
let testObject = {
  hello: "This is my hello string"
};

// Method 1
function propChecker1(obj, prop) {
  if ("undefined" === typeof obj[prop]) {
    // The property DOESN'T exists
    return false;
  } else {
    // The property exists
    return true;
  }
}

// Method 2

function propChecker2(obj, prop) {
  if (obj.hasOwnProperty(prop)) {
    // obj has the hello property
    return true;
  } else {
    // obj doesn't has hello property
    return false;
  }
}

// Method 3

function propChecker3(obj, prop) {
  if (prop in obj) {
    return true;
    // Hello property exists
  } else {
    return false;
    // Hello property does not exist
  }
}

// testing
//Expected result:
//true
//false
//true
//false
//true
//false
console.log(propChecker1(testObject, "hello"));
console.log(propChecker1(testObject, "bye"));
console.log(propChecker2(testObject, "hello"));
console.log(propChecker2(testObject, "bye"));
console.log(propChecker3(testObject, "hello"));
console.log(propChecker3(testObject, "bye"));
