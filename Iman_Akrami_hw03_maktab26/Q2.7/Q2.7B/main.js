console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.7 starts from here:
// ======================================

let library = [
    {
        author: '"Bill Gates"',
        title: '"The Road Ahead"',
        readingStatus: true
    },
    {
        author: '"Steve Jobs"',
        title: '"Walter Isaacson"',
        readingStatus: true
    },
    {
        author: '"uzanne Collin"',
        title: '"SMockingjay: The Final Book of The Hunger Games"',
        readingStatus: false
    }];

function bookReadStatus(inputBookList) {
    let sentenceHolder = [];
    inputBookList.forEach(book => {
        if (book['readingStatus'] === true) {
            book['statement'] = "Already read " + book['title'] + " by " + book['author']
        } else {
            book['statement'] = "You still need to read " + book['title'] + " by " + book['author']
        }
    });
    inputBookList.forEach(function(book){console.log(book['statement'])})
}


bookReadStatus(library)