console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.2 starts from here:
// ======================================

let person = { firstName: "John", lastName: "Doe", age: 50, eyeColor: "blue" }

function objectPropertyLister(obj) {
    let keyArr = []
    for (let key in obj) {
        keyArr.push(key);
    }
    return keyArr;
}

console.log(objectPropertyLister(person));
