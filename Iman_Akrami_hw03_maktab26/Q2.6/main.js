console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.6 starts from here:
// ======================================

let library = [
    {
    title: 'The Road Ahead',
    author: 'Bill Gates',
    libraryID: 1254
    },
    {
    title: 'Walter Isaacson',
    author: 'Steve Jobs',
    libraryID: 4264
    },
    {
    title: 'Mockingjay: The Final Book of The Hunger Games',
    author: 'Suzanne Collins',
    libraryID: 3245
    }];

function objectSortByProperty(arr,prop) {
    arr.sort(function(a, b) { return a[prop]- b[prop];});
    return arr;
}

console.log(objectSortByProperty(library,"libraryID"));