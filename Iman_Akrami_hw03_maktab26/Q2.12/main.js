console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.12 starts from here:
// ======================================

// The test array
let unsortedArray = [4, 3, 1, 10, 11, 2, 3,34,-1,12]

function maxNumFinder(numArray) 
{
    // copy the given array 
    nums = numArray.slice();

    // base case: if we're at the last number, return it
    if (nums.length == 1) { return nums[0]; }

    // check the first two numbers in the array and remove the lesser
    if (nums[0] < nums[1]) { nums.splice(0,1); }
    else { nums.splice(1,1); }

    // with one less number in the array, call the same function
    return maxNumFinder(nums);
}

// testing
console.log(maxNumFinder(unsortedArray));
