console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.4 starts from here:
// ======================================

let person = { firstName: "John", lastName: "Doe", age: 50, eyeColor: "blue" }


// this is the hard way:
function objectLenghtGetter(obj) {
    let len = 0;
    for (let key in obj) {
        len++;
    }
    return len;
}

console.log(objectLenghtGetter(person));

//this is the simple way:

console.log(Object.keys(person).length)

