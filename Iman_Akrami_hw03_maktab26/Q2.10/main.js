console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.10 starts from here:
// ======================================

function is_hexadecimal(str) {

  if (/^[0-9a-fA-F]+$/.test(str)) {
    return true;
  } else {
    return false;
  }
}

console.log(is_hexadecimal("#ffffff"));

console.log(is_hexadecimal("fz5500"));
