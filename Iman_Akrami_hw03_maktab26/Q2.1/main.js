console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.1 starts from here:
// ======================================

let person = { firstName: "John", lastName: "Doe", age: 50, eyeColor: "blue" }

function objectValueLister(obj) {
    let valueArr = []
    for (let key in obj) {
        valueArr.push(obj[key]);
    }
    return valueArr;
}

console.log(objectValueLister(person));
