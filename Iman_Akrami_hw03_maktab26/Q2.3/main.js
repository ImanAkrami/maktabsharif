console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.3 starts from here:
// ======================================

let person = { firstName: "John", lastName: "Doe", age: 50, eyeColor: "blue" }

function objValueKeyPair(obj) {
    let keyValuePairArr = []
    for (let key in obj) {
        keyValuePairArr.push([key,obj[key]]);
    }
    return keyValuePairArr;
}

console.log(objValueKeyPair(person));
