-- 2.1
SELECT Books.BooksTitle FROM Books WHERE (Books.BooksFirstPublishedUnits - Books.BooksSoldUnit) <10
-- 2.2
SELECT Category.CategoryName, Books.BooksTitle, (Books.BooksFirstPublishedUnits-Books.BooksSoldUnit) fw FROM Category,Books 
WHERE Books.BooksCategoryId=Category.idCategory
--2.3
--Creating the view
create view HighestBookAvailableAuthor AS SELECT Books.BooksTitle,Books.BooksInStock,Author.AuthorName,BookAuthored.BookAuthoredIdOfAuthor 
FROM BookAuthored,Books,Author WHERE Books.idBooks = BookAuthored.BookAuthoredIdOfAuthor 
AND Author.idAuthor=BookAuthored.BookAuthoredIdOfAuthor
--Selecting highest number
SELECT MAX(HighestBookAvailableAuthor.BooksInStock) AS Max ,HighestBookAvailableAuthor.BooksTitle , HighestBookAvailableAuthor.AuthorName 
FROM HighestBookAvailableAuthor
--2.4
SELECT Orders.OrdersSoldDate,SUM(Books.BooksPrice) 
FROM Orders INNER JOIN Books ON Orders.OrderIdBook = Books.idBooks WHERE Orders.OrdersSoldDate = GETDATE()
--2.5
SELECT AVG(Books.BooksPrice) AS AverageBookPrice FROM Orders INNER JOIN Books 
ON Orders.OrderIdBook = Books.idBooks WHERE Orders.OrdersSoldDate BETWEEN "2019-07-19" AND "2019-07-29"
--2.6
SELECT Books.BooksTitle,MAX(Books.BooksSoldUnit) AS SoldBooks FROM Books
--2.7
CREATE VIEW SoldBooksWeek AS SELECT BooksTitle,BooksSoldUnit,Orders.OrdersSoldDate FROM Orders INNER JOIN Books ON Books.idBooks = Orders.OrderIdBook
ORDER BY BooksSoldUnit DESC
SELECT * FROM SoldBooksWeek WHERE SoldBooksWeek.OrdersSoldDate BETWEEN "2019-07-19" AND "2019-07-29" LIMIT 3
--2.8
SELECT Books.BooksTitle,Books.BooksPrice FROM Books ORDER BY Books.BooksPrice DESC
--2.9
SELECT * FROM SoldBooksWeek WHERE (SELECT MAX(SoldBooksWeek.BooksSoldUnit) FROM SoldBooksWeek) LIMIT 3