-- MySQL Script generated by MySQL Workbench
-- Wed Sep  4 20:13:38 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Category` (
  `idCategory` INT NOT NULL AUTO_INCREMENT,
  `CategoryName` VARCHAR(45) NULL,
  PRIMARY KEY (`idCategory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Books` (
  `idBooks` INT NOT NULL AUTO_INCREMENT,
  `BooksTitle` VARCHAR(45) NULL,
  `BookAuthors` INT NULL,
  `BooksPublisher` VARCHAR(45) NULL,
  `BooksPublishYear` YEAR NULL,
  `BooksPrice` INT NULL,
  `BooksCategoryId` INT NULL,
  `BooksShelveNumber` INT NULL,
  `BooksRowNumber` INT NULL,
  `BooksFirstPublishedUnits` INT NULL,
  `BooksSoldUnit` INT NULL,
  `BooksInStock` INT NULL,
  `BooksSoldDate` DATE NULL,
  PRIMARY KEY (`idBooks`),
  INDEX `BooksCategoryId_idx` (`BooksCategoryId` ASC) VISIBLE,
  CONSTRAINT `BooksCategoryId`
    FOREIGN KEY (`BooksCategoryId`)
    REFERENCES `mydb`.`Category` (`idCategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Author` (
  `idAuthor` INT NOT NULL AUTO_INCREMENT,
  `AuthorName` VARCHAR(45) NULL,
  PRIMARY KEY (`idAuthor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`BookAuthored`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`BookAuthored` (
  `idBookAuthored` INT NOT NULL AUTO_INCREMENT,
  `BookAuthoredIdOfAuthor` INT NULL,
  `BookAuthoredIdOfBook` INT NULL,
  PRIMARY KEY (`idBookAuthored`),
  INDEX `BookAuthoredIdOfBook_idx` (`BookAuthoredIdOfBook` ASC) VISIBLE,
  INDEX `BookAuthoredIdOfAuthor_idx` (`BookAuthoredIdOfAuthor` ASC) VISIBLE,
  CONSTRAINT `BookAuthoredIdOfBook`
    FOREIGN KEY (`BookAuthoredIdOfBook`)
    REFERENCES `mydb`.`Books` (`idBooks`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `BookAuthoredIdOfAuthor`
    FOREIGN KEY (`BookAuthoredIdOfAuthor`)
    REFERENCES `mydb`.`Author` (`idAuthor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Orders` (
  `idOrders` INT NOT NULL AUTO_INCREMENT,
  `OrdersSoldDate` DATE NULL,
  `OrderIdBook` INT NULL,
  PRIMARY KEY (`idOrders`),
  INDEX `OrderIdBook_idx` (`OrderIdBook` ASC) VISIBLE,
  CONSTRAINT `OrderIdBook`
    FOREIGN KEY (`OrderIdBook`)
    REFERENCES `mydb`.`Books` (`idBooks`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
