let mid_logger = {};
require('winston-daily-rotate-file');
const {
  createLogger,
  format,
  transports
} = require('winston');
const fs = require('fs');
const pathLog = 'logs';
mid_logger.logger = (req, res, next) => {

  if (!fs.existsSync(pathLog)) {
    fs.mkdirSync(pathLog);
  }

  const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${pathLog}/%DATE%-log-res.log`,
    datePattern: 'YYYY-MM-DD'
  });
  const logger = createLogger({
    format: format.combine(
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      format.printf(info => `Time : ${info.timestamp} ${info.level}: ${info.message}`)
    ),
    transports: [
      new transports.Console({
        format: format.combine(
          format.colorize(),
          format.printf(
            info => `${info.timestamp} ${info.level}: ${info.message}`
          )
        )
      }),
      dailyRotateFileTransport
    ]
  });
  logger.info(`id ==> ${req.id} --- ip ==> ${req.ip} --- Host ==> ${req.headers.host} --- Method ==> ${req.method} --- URL ==> ${req.url} --- StatusCode ==> ${res.statusCode} --- User-agent ==> ${req.headers['user-agent']} --- cookie ==> ${req.cookies.name}`);

  next();
}

module.exports = mid_logger;