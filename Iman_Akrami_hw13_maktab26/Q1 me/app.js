const express  = require('express');
const app      = express();
var cookieParser = require('cookie-parser');
app.use(cookieParser());
const httpProxy = require('http-proxy');
const apiProxy = httpProxy.createProxyServer();

const tools = require('./node_tools/logger');

const uuid = require('uuid');
app.use(function (req, res, next) {
    req.id = uuid.v4();
    next();
});

const serverOne = 'http://localhost:3001',
    serverTwo = 'http://localhost:3002',
    serverThree = 'http://localhost:3003';
const morgan = require('morgan');


app.all('*',tools.logger, (req, res) => {
    console.log(tools.logger);
    
    switch (req.originalUrl) {
        case "/app1":
            res.cookie('name', req.headers.host);
            console.log('redirecting to Server1 + ' + req.headers.host);
            apiProxy.web(req, res, {
                target: serverOne
            });
            break;

        case "/app2":
            res.cookie('name', req.headers.host);
            console.log('redirecting to Server2 + ' + req.headers.host );

            apiProxy.web(req, res, {
                target: serverTwo
            });
            break;
        case "/app3":
            res.cookie('name', req.headers.host);
            console.log('redirecting to Server3 + ' + req.headers.host);
            apiProxy.web(req, res, {
                target: serverThree
            });
            break;
    }
    console.log('hmmm');
})


// app.all("/app1*", function(req, res) {
//     console.log('redirecting to Server1');
//     apiProxy.web(req, res, {target: serverOne});
// });

// app.all("/app2*", function(req, res) {
//     console.log('redirecting to Server2');
//     apiProxy.web(req, res, {target: serverTwo});
// });

// app.all("/app3*", function(req, res) {
//     console.log('redirecting to Server3');
//     apiProxy.web(req, res, {target: serverThree});
// });

app.listen(3000, function () {
    console.log('listening on port 3000');
});