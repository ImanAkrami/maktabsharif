const cookieParser = require('cookie-parser');
const session = require('express-session');
const express = require("express");
var bodyParser = require('body-parser');
var fs = require('fs');
// var file = require(__dirname + "/views/data/userData.json");
const app = express();
var username ;
let uid;


app.use(express.static("views/"));
// set the view engine to ejs
app.set("view engine", "ejs");

// use res.render to load up an ejs view file
let userData = require(__dirname + "/views/data/userData.json");

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

// index page
app.get("/", function(req, res) {
  console.log(username);
  
  if (req.session.loggedin) {
    res.render("pages/indexloggedin", {
      userData: userData,
      userName: username
    });
		console.log(req.session.loggedin);
    
	} else {
		res.render("pages/index", {
      userData: userData
    });
    console.log(req.session.loggedin);
	}
	res.end();
  
  
});

app.post('/pch', function(request, response) {
	let changedPass = request.body.chpassword
console.log(changedPass);
console.log(uid);
for (let userInArray = 0; userInArray < userData.length; userInArray++) {
  if (userData[userInArray]['id']== uid) {
    userData[userInArray]['password'] = changedPass;
    console.log(userData[userInArray]['password']);

    fs.writeFile(__dirname + "/views/data/userData.json", JSON.stringify(userData), err => {
      if (err) {
          console.log('Error writing file', err)
      } else {
          console.log('Successfully wrote file')
          request.session.loggedin = fa;
          response.redirect('/');
      }
  })
  }
  
}

 
});

app.post('/auth', function(request, response) {
	username = request.body.username;
  var password = request.body.password;
  

  for (let index = 0; index < userData.length; index++) {

    if (userData[index]['userID'] == username) {
      if (userData[index]['password'] == password) {
        console.log('both are correct');
        request.session.loggedin = true;
        response.redirect('/');
        uid = userData[index]['id']
        break
      }
      else{
        response.render("pages/Login",{
          message: "wrong pass"
        });
        break
      }
    }
    else if (index == userData.length-1){
      response.render("pages/Login",{
        message: "user not found"
      });
      break
    }
  }
});




app.get("/login", function(req, res) {
  res.render("pages/login", {
    message:"Please enter your information"
});
});
app.get("/error", function(req, res) {
  res.render("pages/error");
});


app.listen(8080);
console.log("8080 is the magic port");
console.log();
