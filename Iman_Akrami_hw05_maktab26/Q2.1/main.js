console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.1 starts from here:
// ======================================
// ***********
//this function creates an ARRAY out of the made STRING with the given lenght.
//The Array will be our "race line"
function raceLineCreator(length) {
  let raceLine = "*".repeat(length).split("");
  return raceLine;
}
//this function converts the array to an string for better display in console.
function raceLineDisplayer(racelineArray) {
  console.log(racelineArray.toString());
}
// ***********
//This function creates an array, containing objects for each player.
//here the objects are empty but they will be filled later.
function createPlayersArrayofObject() {
  let players = new Array(+prompt("how Many Players"));
  for (let index = 0; index < players.length; index++) {
    players[index] = {};
  }
  console.log("number of players is " + players.length);
  return players;
}
// ***********
//i made a shuffle function so players turn is random. it shuffles the
//player array for this reason.

function shuffle(Arr) {
  for (let i = Arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [Arr[i], Arr[j]] = [Arr[j], Arr[i]];
  }
  return Arr;
}

// ***********
//this function prompts for each player name and sets the object property of
// 'name', to the user input value
function playerNameGetter(playerArray) {
  shuffle(playerArray);
  for (let player = 0; player < playerArray.length; player++) {
    playerArray[player]["name"] = prompt("Please Enter a Name");
    playerArray[player]["location"] = 0;
  }
  return playerArray;
}

// ***********
//This is the main function. firstly it checks if the movement has exceeded the
//Raceline limits. if so, it means the current player has won. if not,
//it checks if the current player has landed on another player. in this case,
//it will alert the players and move tha old player to starting point.
//lastly, it will move the current player to the new location
//by the given distance, that is generated with a random number generator function.
//while putting the "*" back in the old place of the player.
function playerMover(playerArray, currentPlayerObject, distance, raceLine) {
   currentPlayerObject["location"];
  currentPlayerObject["location"] += distance;
  if (currentPlayerObject["location"] > raceLine.length) {
    winnnerPlayer = currentPlayerObject["name"];
    console.log("player " + winnnerPlayer + " has won!!!");
    winConditionMet = 1;
    return;
  } else if (raceLine[currentPlayerObject["location"]] != "*") {
    for (let index = 0; index < playerArray.length; index++) {
      if (playerArray[index]["location"] === currentPlayerObject["location"]) {
        console.log(
          playerArray[index]["name"] +
          " has been kicked to the starting point!!!"
        );
        playerArray[index]["location"] = 0;
      }
    }
    raceLine[currentPlayerObject["location"]] = currentPlayerObject["name"];
    raceLine[locationOfThePlayerBeforeMoving] = "*";
  } else {
    raceLine[currentPlayerObject["location"]] = currentPlayerObject["name"];
    raceLine[locationOfThePlayerBeforeMoving] = "*";
  }
}
///*************
//this variable holds the win state
let winConditionMet = 0;
let locationOfThePlayerBeforeMoving;
//this loop , runs untill the playerMover() function sets the winConditionMet
// equal to 1. this function call most of the functions we have
//created before and is the main game loop.
function gameLoop() {
  let raceLine = raceLineCreator(300);
  let arrayOfPlayers = playerNameGetter(createPlayersArrayofObject());
  while (winConditionMet != 1) {
    for (let turn = 0; turn < arrayOfPlayers.length; turn++) {
      let randomRoll = Math.floor(Math.random() * 10) + 1;
      alert(
        "it is " +
        arrayOfPlayers[turn]["name"] +
        "'s turn and the dice roll is " +
        randomRoll
      );
      playerMover(arrayOfPlayers, arrayOfPlayers[turn], randomRoll, raceLine);
      raceLineDisplayer(raceLine);
      if (winConditionMet == 1) {
        break;
      }
    }
  }
}
/// ########################### PLAY NOW ########################## ///
//calling the game loop function!
gameLoop();
