// Just to make sure JS is working....

//These Are the given URLs to fetch each JSON from.
let firstJsonURL = "http://api.myjson.com/bins/1b2uu8";
let secondJsonURL = "http://api.myjson.com/bins/xzbj4";
//making an ajax request to fetch first json data
$.ajax({
  type: "get",
  url: firstJsonURL,
  dataType: "json",
  success: function(response) {
    //this variable will hold the first json
    let json1 = response;
    //making an ajax request to fetch the second json data
    $.ajax({
      type: "get",
      url: secondJsonURL,
      dataType: "json",
      success: function(response) {
        //this variable holds the second json data
        let json2 = response;
        //using LODASH library, two jsons are merged
        // mergedJson variable will hold the final data and will be used in all of
        //the script
        mergedJson = _.merge(json1, json2);
        //this variable holds the initial size of the data. or, how many person were in the
        // object. as the add function will use this, and modify this, to create new items.
        letJsonlenCounter = _.size(mergedJson);

        ///===========================CREATEING TABLE=============================
        //this function will use json item KEYS as the header of the table.
        //it will also add the ADD item button
        function tableHeaderCreator() {
          let tableHeader = "";
          tableHeader += "<tr>";
          for (const key in mergedJson[Object.keys(mergedJson)[0]]) {
            tableHeader += "<th>";
            tableHeader += key;
            tableHeader += "</th>";
          }
          //adding the add button to the last header cell.
          tableHeader += '<th><button id="additem">add item</button></th>';
          tableHeader += "</tr>";
          return tableHeader;
        }
        //this function will use Json values to create the body of the table.
        //Also, it will add the EDIT and DELETE buttons.
        function tableContentCreator(json) {
          //getting the keys of each person in an array
          let objectItemKeys = Object.keys(json[Object.keys(json)[0]]);
          let tableBody = "";
          //itterating through object , to add each person data to the cells
          //note that each cell will recieve and specific id.
          for (const person in json) {
            tableBody += "<tr" + ' id="' + person + '" >';
            //itterating through the keys array to get each key/value pair and add it to the table.
            for (let key = 0; key < objectItemKeys.length; key++) {
              tableBody +=
                "<td" + ' id="' + person + objectItemKeys[key] + '" >';
              tableBody += json[person][objectItemKeys[key]];
              tableBody += "</td>";
            }
            //now, adding EDIT and DELETE buttons.
            tableBody += '<td id="' + person + 'CRUD" >';
            tableBody +=
              '<button id="' + person + '%edit-button">edit</button>';
            tableBody +=
              '<button id="' + person + '%delete-button">Delete</button>';
            tableBody += "</tr>";
          }

          return tableBody;
        }

        ///this function adss delete, edit, and functionalities to each button either by running the script on the fly
        // or calling another function.
        function deleteEditFuncAdder() {
          $("button").click(function() {
            if (this.id.split("%")[1] == "delete-button") {
              delete mergedJson[this.id.split("%")[0]];
            } else if (this.id.split("%")[1] == "edit-button") {
              let currentPersonKey = this.id.split("%")[0];
              editRow(mergedJson[currentPersonKey], currentPersonKey);
            } else if ((this.id = "additem")) {
              addPersonFunctionButton();
            }

            tableDrawer();
          });
        }

        //this function combines the tableHeaderCreator and tableContentCreator functions.
        //it will also add some wrapper tags , and functionality to the buttons.
        function tableDrawer() {
          let tb =
            '<table id="mytable">' +
            tableHeaderCreator() +
            tableContentCreator(mergedJson) +
            "</table>";
          $("#tablecontainer").html(tb);
          deleteEditFuncAdder();
        }
        //calling the table draw function for the first time...
        tableDrawer();
        //this function, recieves the current person object and its key in the main json,
        //and opens up edit page overlay and populates it with the default data.
        //it will also create buttons for confirm and edit
        function editRow(personObject, currentPersonKey) {
          //first, show the overlay.
          $("#edit-overlay").css("visibility", "visible");
          let editContainer = '<div id="edit-container">';
          //adding h5 elements based on person object keys, and then adding an editable text input
          //for the user to edit them out.
          for (const key in personObject) {
            editContainer += "<h5>" + key + "</h5>";
            editContainer +=
              '<input type="text"' +
              ' value="' +
              personObject[key] +
              '" >' +
              "</input>";
          }
          //wrapping it up...
          editContainer += "</div>";
          //adding the buttons.
          //first adding confirm button
          editContainer +=
            '<input id="confirm-edit" type="submit" value="Submit" ></input>';
          //now adding cancel button.
          editContainer +=
            '<input id="cancel-edit" type="submit" value="Cancel" ></input>';
          //setting the overlay html content to the generated form.
          $("#edit-overlay").html(editContainer);
          //now, adding functionality to each button.
          //confirmEditButton to the edit confirm button...
          $("#confirm-edit").click(function(e) {
            confirmEditButton(currentPersonKey);
          });
          // and just hiding the edit overlay if the user clicks cancel.
          $("#cancel-edit").click(function(e) {
            $("#edit-overlay").css("visibility", "hidden");
          });
        }
        //confirmEditButton gets the value of all inputs, and sets them to the
        //corresponding keys in the current person object, closes the edit overlay div,  then, it redraws the table.
        function confirmEditButton(currentPersonKey) {
          let formLenght = $("#edit-container :input").length;

          for (let formItem = 0; formItem < formLenght; formItem++) {
            mergedJson[currentPersonKey][
              Object.keys(mergedJson[currentPersonKey])[formItem]
            ] = $("#edit-container :input")[formItem].value;
          }
          //hiding edit overlay div
          $("#edit-overlay").css("visibility", "hidden");
          //redrawing the table.
          tableDrawer();
        }

        //this functions , first creates and displayes the edit section div. it won't byt itself add anydata to
        //the json or the table. that, will be accomplished by another function if the user clicks confirm button.
        function addPersonFunctionButton() {
          //toggle the add item container by sliding it down and up.
          $("#add-item-container").slideToggle();
          //creating the add form...
          let itemObjectKeys = Object.keys(
            mergedJson[Object.keys(mergedJson)[0]]
          );
          let addContainerHTML = "";
          for (let innerKey = 0; innerKey < itemObjectKeys.length; innerKey++) {
            addContainerHTML += "<h3>" + itemObjectKeys[innerKey] + "</h3>";
            addContainerHTML +=
              '<input type="text" id="addItem%' +
              itemObjectKeys[innerKey] +
              '"></input>';
          }
          //creating the addConfirm button...
          addContainerHTML += '<button id="addConfirm" >Confirm</button>';
          //and then setting the html of the add container to the generated string.
          $("#add-item-container").html(addContainerHTML);
          //this is the actual adding the new data to the table.
          $("#addConfirm").click(function() {
            //first, we add one to the main json size counter. because we want the new person id to start from the last.
            letJsonlenCounter++;
            let newPersonKey = "person" + letJsonlenCounter;
            //getting all input data as a list
            let addFormData = $("#add-item-container :input");
            //creating an empty object for the new person.
            let newPersonObject = {};
            //adding key/valye pair to the empty object of the new person.
            //note that i've subtracted 1 form the array lenght. because by design,
            //the last button also counts as an input!
            for (let keys = 0; keys < addFormData.length - 1; keys++) {
              currentFormDataKey = addFormData[keys].id.split("%")[1];
              newPersonObject[currentFormDataKey] = addFormData[keys].value;
            }
            //and now, the new person object will be added to the main Json data...
            mergedJson[newPersonKey] = newPersonObject;
            //and we redraw the table.
            tableDrawer();
          });
        }

      }
    });
  }
});
