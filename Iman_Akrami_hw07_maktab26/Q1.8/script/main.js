// Just to make sure JS is working....
console.log("connected");
//Defining the countries Information on a higher level so i can access it from anywhere.
//this variable will contain ALL of the countries information.
let arrOfCountryInfo;
///========================== ON OPENING THE PAGE =====================
//making an Ajax request to the destination. it will return a string, containing an ARRAY of OBJECTS.
$.ajax({
  type: "get",
  url: "https://restcountries.eu/rest/v2",
  dataType: "text",
  success: function (response) {
    //on success, converting the returend string to an actual object.
    arrOfCountryInfo = JSON.parse(response);
    //just to make sure the array came back . i'll comment this out later.
    console.log(arrOfCountryInfo[0]);
    //Hiding everything except the dropdown menu. because i don't have time to style it without information. i'm not lazy!!!
    $(".top").hide();
    $(".middle").hide();
    $(".bottom").hide();
    //Now, i'm going to get the names of each country and add it as OPTION text, for conviniece though, i will use the capitals
    // as value. it will come in handy later.
    let option = "";
    for (let i = 0; i < arrOfCountryInfo.length; i++) {
      option +=
        '<option value="' +
        arrOfCountryInfo[i]["capital"] +
        '">' +
        arrOfCountryInfo[i]["name"] +
        "</option>";
    }
    //okay! i have all my options. now appedning it to the dropdown menu.
    $("#country-list").append(option);

    ///========================== ON USER SELECTING AN ITEM =====================
    // this is a function that executes when the user selects an item.
    $("#country-list").change(function () {
      //this variable holds the flagimage url. i could use it directly in the attr method, but it is cleaner this way.
      let flagImage =
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["flag"];
      //chaning the image of the country flag container.
      $("#country-flag").attr("src", flagImage);
      // here, i'll access the arrOfCountryInfo Array, while getting the selected item and then accessing each
      //text field required information. the tag name and object keys should be self explanetory.
      $("#calling-code-number").text(
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["callingCodes"][0]
      );
      $("#countryname").text(
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["name"]
      );
      $("#native-name").text(
        "Native Name: " +
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["nativeName"]
      );
      $("#capital").text(
        "Capital: " +
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["capital"]
      );
      $("#region").text(
        "Region: " +
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["region"]
      );
      $("#population").text(
        "Population: " +
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["population"]
      );
      $("#language").text(
        "Main Language: " +
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["languages"][0][
        "name"
        ]
      );
      $("#time-zones").text(
        "Main TimeZone: " +
        arrOfCountryInfo[$("#country-list")[0].selectedIndex]["timezones"][0]
      );

      ///================MapCoordinateCreator==================
      // NOTE : GOOGLE MAP API IS NOW LIMITED. I WON'T IMPLEMENT IT IN THIS VERSION. I FOUND A WORKAROUND BUT
      //I DON'T HAVE TIME TO USE IT FOR NOW.
      // let coordLat = arrOfCountryInfo[$("#country-list")[0].selectedIndex]['latlng'][0]
      // let coordLng = arrOfCountryInfo[$("#country-list")[0].selectedIndex]['latlng'][1]
      // let firstParamForMapFrame = coordLat + 10
      // let secondParamForMapFrame = coordLng + 5
      // let thirdParamForMapFrame = coordLat - 10
      // let forthParamForMapFrame = coordLng - 5
      // // $(selector).html(htmlString);
      // let mapCoordinatesURL = '<iframe id="map" scrolling="no" src="https://www.openstreetmap.org/export/embed.html?bbox=' + firstParamForMapFrame + '%2C'+ secondParamForMapFrame + '%2C' + thirdParamForMapFrame + '%2C' + forthParamForMapFrame + '&amp;layer=mapnik&amp;marker=' + coordLat + '%2C' + coordLng +  '" style="border: 1px solid black"></iframe><br/><a><a href="https://www.openstreetmap.org/?mlat=35.521&amp;mlon=51.385#map=4/15.521/51.385">View Larger Map</a></a>'
      // console.log(mapCoordinatesURL)
      // $(".bottom").html(mapCoordinatesURL);
      //===================WEATHER INFORMATION ======================
      //getting the capital naame, because i can use it in the url to make an ajax request.
      let status = this.value;
      ///creating the url request.
      let urlForPostWeatherInfo =
        "http://api.openweathermap.org/data/2.5/weather?q=" +
        status +
        "&APPID=b871ae396016494299631733669d0a17";
      // console.log(urlForPostWeatherInfo);
      $.ajax({
        type: "get",
        url: urlForPostWeatherInfo,
        dataType: "text",
        success: function (response) {
          //first i'll convert it into an objext
          let responseConvertedToObject = JSON.parse(response);
          //filling the containers, same as before.
          $("#weather-desc").text(
            responseConvertedToObject["weather"][0]["main"]
          );
          $("#weather-windspeed").text(
            responseConvertedToObject["wind"]["speed"] + " m/s"
          );
          $("#weather-temp").text(
            responseConvertedToObject["main"]["temp"] - 273.15 + "c"
          );
          $("#weather-humidity").text(
            responseConvertedToObject["main"]["humidity"] + "%"
          );
          $("#weather-visibility").text(
            responseConvertedToObject["visibility"] + " m"
          );
          //now, i'll display the hidden sections which are now filled with information.
          $(".top").show();
          $(".middle").show();
          $(".bottom").show();
        }
      });
    });
  }
});

//TADA!
