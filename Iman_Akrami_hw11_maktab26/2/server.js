console.log("connected and doing stuff!");


// const mongo = require("mongodb");
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017/MaktabAkramiDB2";

//First We Create the Database using db method, and then create employee collection.
console.log("connected");
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  // console.log("Database created!");
  let dataBaseMine = db.db("MaktabAkramiDB2");
  dataBaseMine.createCollection("employees", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

//Adding one record to the Database

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  let dataBaseMine = db.db("MaktabAkramiDB2");
  let employees = {
    firstName: "Iman",
    lastName: "Akrami",
    NationalID: "3621898697",
    birthDate: new Date("Sep 29,1989"),
    gender: "male"
  };
  dataBaseMine.collection("employees").insertOne(employees, function(err, res) {
    if (err) throw err;
    console.log("one employee added using insertone method");
    db.close();
  });
});

//Adding mote than one record to the Database

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  let dataBaseMine = db.db("MaktabAkramiDB2");
  let employees = [
    {
      firstName: "Ehsan",
      lastName: "Akrami",
      NationalID: "123456789",
      birthDate: new Date("Sep 21,1992"),
      gender: "male"
    },
    {
      firstName: "Sina",
      lastName: "Masoumi",
      NationalID: "987654321",
      birthDate: new Date("Sep 21,1993"),
      gender: "male"
    },
    {
      firstName: "Ali",
      lastName: "Alian",
      NationalID: "569874123",
      birthDate: new Date("feb 21,1992"),
      gender: "male"
    }
  ];
  dataBaseMine
    .collection("employees")
    .insertMany(employees, function(err, res) {
      if (err) throw err;
      console.log("multiple records inserted");
      db.close();
    });
});

// Read Items From database including ID
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  let dataBaseMine = db.db("MaktabAkramiDB2");
  dataBaseMine
    .collection("employees")
    .find({})
    .toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
      db.close();
    });
});

//deleting a single record 
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    let dataBaseMine = db.db("MaktabAkramiDB2");
    //using deleteone method
    dataBaseMine.collection("employees").deleteOne({"NationalID":"3621898697"}, function(err, obj) {
      if (err) throw err;
      console.log("1 record deleted");
      db.close();
    });
  });

//delete multiple items, from db
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    let dataBaseMine = db.db("MaktabAkramiDB2");
    //using deleteMany method
    dataBaseMine.collection("employees").deleteMany({"firstName":{$in: ["Ehsan", "Sina", "Ali"]}}, function(err, obj) {
      if (err) throw err;
    //   console.log(obj.result.n + " record(s) deleted");
      db.close();
    });
});

//updating item ( a single one)

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    let dataBaseMine = db.db("MaktabAkramiDB2");
    //using updateOne Method
    dataBaseMine.collection("employees").updateOne({ firstName: "Iman" }, { $set: {lastName: "Alavian"} }, function(err, res) {
      if (err) throw err;
      console.log("a single record updated");
      db.close();
    });
  });

//updateing multiple records from db
MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dataBaseMine = db.db("MaktabAkramiDB2");
    //using updateMany method
    dataBaseMine.collection("employees").updateMany(
        {"firstName":{$in: ["Iman", "Ehsan", "Ali"]}}
        , {
        $set: {
            lastName: "Akrami",
            NationalID: "000000000"
        }
    }, function (err, res) {
        if (err) throw err;
        // console.log(res.result.nModified + " records updated");
        db.close();
    });
});
