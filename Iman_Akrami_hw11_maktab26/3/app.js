var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/Ex4MaktabAkrami", {
  useNewUrlParser: true
});


// This part includes CRUD functions for company.

// CREATE for company========================================

let company = require('./models/company');
//console.log(company);
let Mokhaberat = new company({
  companyName: "Mokhaberat company",
  companyIdCode: "A123",
  city: "tehran",
  province: "tehran",
  registerDate: new Date(2012, 1, 1),
  phoneNumber: 9396561733
});
Mokhaberat.save();



let faraPak = new company({
  companyName: "power2",
  companyIdCode: "B123",
  city: "tehran",
  province: "tehran",
  registerDate: new Date(2017, 1, 1),
  phoneNumber: 9396561733
});
faraPak.save();



let someOtherCompany = new company({
  companyName: "power3",
  companyIdCode: "C123",
  city: "tehran",
  province: "tehran",
  registerDate: new Date(2018, 2, 2),
  phoneNumber: 9396561733
});
someOtherCompany.save();



// READ for company========================================

company.find({}, (err, findCompany) => {
  if (err) console.log(err);
  console.log(findCompany);
})

company.find({
  companyIdCode: "BBB123"
}, (err, findCompany) => {
  if (err) console.log(err);
  console.log(findCompany);
})


// UPDATE for company=========================one

company.findOneAndUpdate({
  companyName: "power3"
}, {
  companyIdCode: "BBB123"
}, (err, companyUpdate) => {
  if (err) console.log(err);
  console.log(companyUpdate);
})




// DELETE for Company=======================one

company.findOneAndDelete({
  companyName: "power3"
}, (err, companyRemove) => {
  if (err) throw err
  console.log(companyRemove);

})
///////////////////////////////////////////////////////////////////////////////////////////////////

// This part includes CRUD functions for employee
// CREATE for Employee=======================

let employees = require('./models/employees');
console.log(employees);
let emp1 = new employees({
  firstName: "Iman",
  lastName: "Akrami",
  city: "Sari",
  gender: "male",
  nationalCode: 3621898697,
  companyId: Mokhaberat._id,
  positionIfManager: true,
  birthDate: "Feb 21,1989"
})
emp1.save();
let emp2 = new employees({
  firstName: "ali",
  lastName: "alian",
  city: "tehran",
  gender: "male",
  nationalCode: 123456789,
  companyId: faraPak._id,
  positionIfManager: true,
  birthDate: "Feb 21,1992"
})
emp2.save();
let emp3 = new employees({
  firstName: "Sina",
  lastName: "Masoumi",
  city: "tehran",
  gender: "male",
  nationalCode: 0000000000,
  companyId: someOtherCompany._id,
  positionIfManager: true,
  birthDate: "Feb 21,1992"
})
emp3.save();
let emp4 = new employees({
  firstName: "Mamal",
  lastName: "Alex",
  city: "tehran",
  gender: "male",
  nationalCode: 1111111111,
  companyId: faraPak._id,
  positionIfManager: false,
  birthDate: "Feb 21,1992"
})
emp4.save();
let emp5 = new employees({
  firstName: "Nazi",
  lastName: "Nazian",
  city: "tehran",
  gender: "female",
  nationalCode: 2222222222,
  companyId: Mokhaberat._id,
  positionIfManager: false,
  birthDate: "Feb 21,1992"
})
emp5.save();

// READ for Employee=======================


employees.find({}, (err, findemployees) => {
  if (err) console.log(err);
  console.log(findemployees);
})


// UPDATE for Employee=======================


employees.findOneAndUpdate({
  firstName: "maryam"
}, {
  lastName: "moradi"
}, (err, employeesUpdate) => {
  if (err) console.log(err);
  console.log("employee item has been updated");
})


// DELETE for Employee=======================

employees.findOneAndDelete({
  firstName: "maryam"
}, (err, employeesRemove) => {
  if (err) throw err
  console.log("employee has been removed");

})

//


// part 3.3
employees.find(companyName: "Mokhaberat company"
}, (err, findEmpCom) => {
if (err) console.log(err);
console.log(findEmpCom);

})

// part 3.3 AGAIN!
company.find({
  registerDate: {
    $lt: '2019-1-1',
    $gte: '2018-1-1'
  }
}, 'companyName', function (err, res) {
  if (err) console.log(err);
  console.log(res);
})


// part 3.4
employees.find({
  companyName: "Mokhaberat company",
  positionIfManager: "true"
}, function (err, res) {
  if (err) console.log(err);
  console.log(res);
})


// part 3.5

//except id//
employees.find({
  birthDate: {
    $gte: '1989-1-1',
    $lte: '1999-1-1'
  }
}, {
  _id: 0
}, function (err, res) {
  if (err) console.log(err)
  console.log(res)
})



//part 3.6


employees.find({
  positionIfManager: "true"
}, function (err, res) {
  if (err) console.log(err);
  console.log(res);
})


// part 3.7

employees.find({
    positionIfManager: true
  }, ["firstName", "lastName"])
  .populate('companyId', 'companyName')
  .exec(function (err, res) {
    console.log(res);

  });


//part 3.8

company.updateMany({
  city: 'tehran',
  province: 'tehran'
}, function (err, res) {
  if (err) console.log(err)
  console.log(res.nModified + "updated.")
})

// END of EXCERSIE!///////////*************************************/// */
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;