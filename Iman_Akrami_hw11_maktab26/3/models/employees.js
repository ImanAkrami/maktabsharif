//loading the required module
const mongoose= require('mongoose');
//creating the schema
const Schema = mongoose.Schema;

const employeesSchema= new Schema({
    firstName:{
         type:String,
         required: true,
         unique:false,
         maxlength:40,
         minlength:3
        },
    lastName:{
        type:String,
        required: true,
        unique:false,
        maxlength:40,
        minlength:3
        },
    sex: {
        type:String,
        required: true,
        unique:false,
        maxlength:6,
        minlength:4
        },
    nationalCode: {
        type:String,
        required: true,
        unique:false,
        maxlength:10,
        minlength:10
        },
    companyId:{
            type:Schema.Types.ObjectId, 
            ref:'company',
            required:true
        },    
    position: {
        type:Boolean,
        required: true,
        unique:false
        },
    birthDate: {
        type:Date,
        required: true,
        unique:false,
        }
})
//exporting so i can use it in the main app
module.exports=mongoose.model('employees', employeesSchema);