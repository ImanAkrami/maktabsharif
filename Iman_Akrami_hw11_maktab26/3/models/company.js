//loading the required module

const mongoose = require("mongoose");

//creating the schema

const Schema = mongoose.Schema;

const companySchema = new Schema({
  companyName: {
    type: String,
    required: true,
    unique: false,
    maxlength: 40,
    minlength: 3
  },
  companyIdCode: [
    {
      type: String,
      required: true,
      unique: true
    }
  ],
  city: {
    type: String,
    required: true,
    unique: false,
    maxlength: 50,
    minlength: 3
  },
  province: {
    type: String,
    required: true,
    unique: false,
    maxlength: 50,
    minlength: 1
  },
  registerDate: {
    type: Date,
    required: true,
    unique: false
  },
  phoneNumber: {
    type: Number,
    required: true,
    unique: false,
    maxlength: 13,
    minlength: 10
  }
});

module.exports = mongoose.model("company", companySchema);
