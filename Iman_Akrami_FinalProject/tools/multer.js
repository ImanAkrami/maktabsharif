var multer = require("multer");
var storage = multer.diskStorage({
    destination: function(req, file, callback){
        console.log('2');
        
        callback(null, './public/uploads'); // set the destination
    },
    filename: function(req, file, callback){
        callback(null, Date.now() + '.jpg'); // set the file name and extension
    }
});
var upload = multer({storage: storage});

module.exports = upload