
const chalk = require('chalk');
const mongoose = require('mongoose');

async function mongoDBconnect(url){
    try {
    await mongoose.connect(url,{useNewUrlParser:true,useUnifiedTopology: true});
        console.log(chalk.white.bgGreen.underline.bold('Connected To MongoDB'));
    } catch (error) {
        console.log(chalk.white.bgRed.underline.bold('Not Able To Connect to MongoDB'),error);
    }


}
module.exports = mongoDBconnect;

