const chalk = require("chalk");
const Article = require("../models/articlemodel");

function getAllArticles(req, res) {
  console.log(
    chalk.red.bgGreen.underline.bold("responding to GET ALL ARTICLE api call")
  );
  Article.find()
    .populate("author_ID")
    .exec((err, articles) => {
      if (err) {
        res.send(err);
      } else {
        // router.set('views', path.join(__dirname, 'views'));
        return res.send(articles);
      }
    });
}
module.exports = getAllArticles;
