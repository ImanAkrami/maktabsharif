const chalk = require("chalk");
const Comment = require("../../models/commentmodel");

function adminDeleteComment(req, res) {
  console.log(
    chalk.red.bgGreen.underline.bold(
      "removing comment with the id of " + req.query.id
    )
  );

  Comment.findOneAndDelete({
      _id: req.query.id,

  }, (err, result) => {

      if (err) return res.json({
          success: false,
          msg: 'Unable To Delete Comment'
      })
      return res.redirect('back');
  })
}
module.exports = adminDeleteComment;