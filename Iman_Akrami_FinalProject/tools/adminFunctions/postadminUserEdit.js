const chalk = require('chalk');
const User = require('../../models/usermodel');

function adminEditUserGet(req, res) {
    console.log(chalk.white.bgGreen.underline.bold('trying to get user info for admin to edit'));
    console.log(req.query.id);
    
    User.findOne({
        _id: req.query.id
    }, {

        __v: 0,
        created_at: 0,
        update_at: 0,
        role:0,
        password:0

    }, async (err, users) => {

        if (err) return res.json({
            success: false,
            msg: err
        })
        return res.json(users);
    })
}
module.exports = adminEditUserGet;