const chalk = require('chalk');
const User = require('../../models/usermodel');

function userInfoGetter(req, res) {
     if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.sampleFile;
  let sampleFilePath = 'public/images/'+Date.now()+ sampleFile.name
  // Use the mv() method to place the file somewhere on your server
  
  console.log(req.body);
  
  sampleFile.mv( sampleFilePath , function(err) {
    if (err)
      return res.status(500).send(err);

    User.findOneAndUpdate({
        username: req.user.username
    }, {
        $set: {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            username: req.body.username,
            mobile: req.body.mobile,
            password: req.body.password,
            gender: req.body.gender,
            avatar:sampleFilePath.slice(7) 
        }
    }, (err, result) => {
            if (err) {
                res.send(err)
                
            }
    })
    res.redirect('../../adminpanel')
  });
    
 
}
module.exports = userInfoGetter;