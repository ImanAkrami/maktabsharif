const chalk = require('chalk');
const User = require('../../models/usermodel');

function userInfoGetter(req, res) {
    console.log(chalk.red.bgGreen.underline.bold('trying to get user info'));

    
    User.find({
        role: "user",
    }, {
        __v: 0,
        created_at: 0,
        update_at: 0,
        role:0,

    }, async (err, users) => {

        if (err) return res.json({
            success: false,
            msg: 'can not get data'
        })
        return res.status(200).json(users);
    })
}
module.exports = userInfoGetter;