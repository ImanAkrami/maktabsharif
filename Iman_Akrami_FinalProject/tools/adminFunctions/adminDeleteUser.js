const chalk = require("chalk");
const Article = require("../../models/articlemodel");
const User = require("../../models/usermodel");
const Comment = require("../../models/commentmodel");

function getUserArticleEdit(req, res) {
  console.log(
    chalk.white.bgGreen.underline.bold(
      "Removing USER and its POSTS with the id of " + req.query.id
    )
  );
  Article.deleteMany(
    {
      author_ID: req.query.id
    },
    (err, result) => {
      if (err)
        return res.json({
          success: false,
          msg: err
        });

      console.log(
        chalk.white.bgGreen.underline.bold(
          "User Posts removed. now removing Comments..." + req.query.id
        )
      );
      Comment.deleteMany(
        {
          author_ID: req.query.id
        },
        (err, result) => {
          if (err)
        return res.json({
          success: false,
          msg: err
        });
          console.log(
            chalk.white.bgGreen.underline.bold(
              "User Comments removed. now removing User..." + req.query.id
            )
          );
          User.findOneAndDelete(
            {
              _id: req.query.id
            },
            (err, result) => {
              if (err)
                return res.json({
                  success: false,
                  msg: err
                });
              return res.redirect("back");
            }
          );
        }
      );
    }
  );
}
module.exports = getUserArticleEdit;
