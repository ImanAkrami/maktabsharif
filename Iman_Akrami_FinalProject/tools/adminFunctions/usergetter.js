const User = require('../../models/usermodel');
const chalk = require('chalk');


async function getUsers(req, res, next) {

    console.log(chalk.white.bgYellow.underline.bold('trying get user listing'));

    const users = await User.find({role:'user'},'-password');
    console.log(chalk.red.bgGreen.underline.bold('user GET successful'));

    return res.send(users);

}
module.exports = getUsers;