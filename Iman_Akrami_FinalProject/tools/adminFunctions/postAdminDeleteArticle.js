const chalk = require("chalk");
const Article = require("../../models/articlemodel");

function getUserArticleEdit(req, res) {
  console.log(
    chalk.red.bgGreen.underline.bold(
      "REMOVING article with the id of " + req.query.id
    )
  );

  Article.findOneAndDelete({
      _id: req.query.id,

  }, (err, result) => {

      if (err) return res.json({
          success: false,
          msg: 'Unable To Delete Article'
      })
      return res.redirect('../myposts');
  })
}
module.exports = getUserArticleEdit;