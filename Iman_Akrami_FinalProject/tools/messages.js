let messages = {
  defaults: {
    registerform: {
      state: false,
      text: "Please fill in the form"
    },
    loginready: {
      state: false,
      text: "You can login now!"
    },
    loginuser: {
      state: false,
      text: "Please enter your credintials"
    }
  },
  errors: {
    alreadyexists: {
      state: false,
      text: "User already exists"
    }
  },
  temps: {
    singlepostid: ""
  }
};
module.exports = messages;
