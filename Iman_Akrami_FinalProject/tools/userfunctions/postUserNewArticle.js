const chalk = require('chalk');
const Article = require('../../models/articlemodel');

function userNewPost(req, res) {
  console.log(chalk.red.bgGreen.underline.bold('Trying to add new post'));
  
     if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
    }
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.sampleFile;
  let sampleFilePath = 'public/images/'+Date.now()+ sampleFile.name
  // Use the mv() method to place the file somewhere on your server
  
  
  sampleFile.mv( sampleFilePath , function(err) {
    if (err)
      return res.status(500).send(err);
      console.log(req.user._id);
      
      console.log(chalk.red.bgGreen.underline.bold('Article image uploaded okay'));
      new Article({
        author_ID: req.user._id,
        articleTitle: req.body.title,
        articleContent: req.body.content,
        articleImage: sampleFilePath 
    }).save((err, article) => {
      console.log(article);

      console.log(chalk.red.bgGreen.underline.bold('trying to save the article'));
        
        if (err) return res.json({
            success: false,
            msg: err
        });

        res.redirect('back')
      })

  });
    
 
}
module.exports = userNewPost;