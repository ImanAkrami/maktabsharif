const chalk = require('chalk');
const User = require('../../models/usermodel');

function getUserArticles(req, res) {
    console.log(chalk.red.bgGreen.underline.bold('trying to get user articles'));

    
    User.findOne({
        role: "user",
        username: req.user.username
    }, {
        _id: 0,
        __v: 0,
        created_at: 0,
        update_at: 0,
        password:0,
        role:0,

    }, async (err, users) => {

        if (err) return res.json({
            success: false,
            msg: 'can not get data'
        })
        return res.json({
            success: true,
            users
        });
    })
}
module.exports = getUserArticles;