const chalk = require("chalk");
const User = require("../../models/usermodel");

function userDeleter(req, res, next) {
  console.log(chalk.red.bgGreen.underline.bold("trying to delete users"));

  User.findOne(
    {
      username: req.user.username
    },
    {
      _id: 0,
      __v: 0,
      created_at: 0,
      update_at: 0,
      role: 0
    },
    async (err, users) => {
      if (err) {
        return res.json({
          success: false,
          msg: "can not get data"
        });
      } else {
        if (req.body.password == users.password) {
          User.findOneAndDelete({ username: req.user.username }).then(() => {
            req.logout()
            res.redirect('../../../logout')
            
          });
        } else {
            
          res.redirect('../../userpanel')
        }
        // return res.json({
        //   success: true,
        //   users
        // });
      }
    }
  );
}
module.exports = userDeleter;
