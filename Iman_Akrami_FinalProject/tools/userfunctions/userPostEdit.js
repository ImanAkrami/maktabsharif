const chalk = require("chalk");
const Article = require("../../models/articlemodel");

function getUserArticleEdit(req, res) {
  console.log(
    chalk.red.bgGreen.underline.bold(
      "Getting Article to EDIT with id of " + req.query.id
    )
  );
  Article.findOne({
      _id: req.query.id,

  }, (err, result) => {
        console.log(result);
        
      if (err) return res.json({
          success: false,
          msg: 'can not get data'
      })
      return res.render('usereditarticle',{article:result});
  })

  // Article.findOneAndDelete({
  //     _id: req.query.id,

  // }, (err, result) => {

  //     if (err) return res.json({
  //         success: false,
  //         msg: 'can not get data'
  //     })
  //     return res.redirect('./myposts');
  // })
}
module.exports = getUserArticleEdit;
