const chalk = require("chalk");
const Article = require("../../models/articlemodel");

function userInfoGetter(req, res) {
  console.log(chalk.red.bgGreen.underline.bold("trying to get user info"));
  let userInfo = req.user;
  Article.find({
    author_ID: req.user._id
  }).populate("author_ID")
    .exec((err, articles) => {
      if (err) {
        res.send(err);
      } else {
        // router.set('views', path.join(__dirname, 'views'));
        return res.send(articles);
      }
    });
}
module.exports = userInfoGetter;
