const chalk = require("chalk");
const Article = require("../../models/articlemodel");

function postUserEditArticles(req, res) {

    

  console.log(
    chalk.red.bgGreen.underline.bold(
      "Applying Article Edit with the id of " + req.query.id
    )
  );
  let sampleFile = req.files.sampleFile;
  let sampleFilePath = 'public/images/'+Date.now()+ sampleFile.name
  
  sampleFile.mv( sampleFilePath , function(err) {
    Article.findOneAndUpdate({
        _id: req.query.id,
  
    },
    {
      $set: {
          articleTitle: req.body.title,
          articleContent: req.body.content,
          articleImage: sampleFilePath,
          update_at: Date.now(),
      }
    },(err,result)=>{
        res.redirect('../myposts')
    }
    )
});
  
 

}
module.exports = postUserEditArticles;
//===

//===
