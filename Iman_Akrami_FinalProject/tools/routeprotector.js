const ac = {};
const User = require('../models/usermodel');

ac.routeController = (roles) => {
    return (req, res, next) => {
        if (roles.includes(req.user.role)){
        return next()}
        else 
        {return res.status(403).render('error',{message:' You do not have access to this resources'})}
    };
};



module.exports = ac;