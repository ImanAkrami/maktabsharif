const chalk = require("chalk");
const Article = require("../models/articlemodel");
const Comment = require("../models/commentmodel");
const User = require("../models/usermodel");

function getAllComments(req, res) {
  console.log(chalk.red.bgGreen.underline.bold("comments for article with the id of " + req.params.id));
 
  Comment.find({
    article_ID: req.query.id
  }).populate("author_ID")
    .exec((err, comments) => {
      if (err) {
        res.send(err);
      } else {
        // router.set('views', path.join(__dirname, 'views'));
        return res.send(comments);
      }
    });
}
module.exports = getAllComments;
