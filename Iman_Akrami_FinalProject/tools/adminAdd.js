const User = require('../models/usermodel');
const chalk = require('chalk');
function adminAdder(req, res, next) {

    User.findOne({role: 'admin'}, (err, existAdmin) => {
      
        if (err) return res.json({success: false, msg: err});
        if (existAdmin) return res.status(404).send('Admin already exists');
        console.log(chalk.red.bgYellow.underline.bold('trying to add Admin'));
        
        
        new User({
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          password: req.body.password,
          role: 'admin',
          gender: req.body.gender,
          username: req.body.username,
          mobile:req.body.mobile
        }).save((err, user) => {
          if (err) return res.json({success: false, msg: err});
          console.log(chalk.white.bgGreen.underline.bold('Admin added'));
          return res.json({success: true, user});
        });
    
      });
}
module.exports = adminAdder;