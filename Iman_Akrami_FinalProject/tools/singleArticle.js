//this is general API file
let express = require("express");
let router = express.Router();
const Article = require('../models/articlemodel');
const Users = require('../models/usermodel');
const messages = require("./messages");

function singlearticle(req, res, next) {

    console.log(chalk.red.bgYellow.underline.bold('single article api call'));
    console.log('new'+ messages.temps.singlepostid);
    
    Article.findOne({
        _id: messages.temps.singlepostid
    }, {
        _id: 0,
        __v: 0,
        update_at: 0,

    }, async (err, article) => {

        if (err) return res.json({
            success: false,
            msg: 'can not get data'
        })
        return res.json({
            success: true,
            article
        });
    })
}
module.exports = singlearticle;

