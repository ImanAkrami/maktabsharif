const User = require('../models/usermodel');
const chalk = require('chalk');
const messages = require('../tools/messages');



function userAdd(req, res, next) {

    
    User.findOne({username:req.body.username}, (err, userExists) => {
        console.log(chalk.red.bgYellow.underline.bold('trying to add user'));
        if (err) return res.json({success: false, msg: err});
        
        if (userExists) {
          
          messages.defaults.registerform.state = true
          return res.status(200).redirect('../register')
        };
        

        new User({
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          username: req.body.username,
          password: req.body.password,
          gender: req.body.gender,
          mobile: req.body.mobile,
          role:'user',
          avatar:"",
          created_at: Date.now()
        }).save((err, user) => {
          if (err) {
            console.log(chalk.white.bgRed.underline.bold('Failed to add user'));
            return res.json({success: false, msg: err})
          };
          console.log(chalk.white.bgGreen.underline.bold('User Added!'));
          messages.defaults.loginready.state = true
          res.status(200).redirect('../login')
        });
    
      });
}
module.exports = userAdd;