//this is route handler for single post displaying, as it should be accessed without
//logging in
//first makes a query to the data base based on query string id, and then populates it with comments and
//user info
let express = require("express");
let router = express.Router();
const Article = require("../models/articlemodel");
const Comment = require("../models/commentmodel");
var path = require("path");
const chalk = require("chalk");
/* handle general api requests*/
router.get("/:id", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  Article.findOneAndUpdate(
    {
      _id: req.params.id
    },
    {
      $inc: { views: 1 }
    }
  )
    .populate("author_ID", { password: 0, username: 0, gender: 0, mobile: 0 })
    .exec((err, article) => {
      if (err) {
        res.send(err);
      } else {
        // router.set('views', path.join(__dirname, 'views'));
        return res.render("usersinglearticle", { articleinfo: article });
      }
    });
});
router.post("/:id", function(req, res, next) {
  console.log("Trying to ADD comment");

  new Comment({
    author_ID: req.user._id,
    article_ID: req.params.id,
    commenttitle: req.body.commenttitle,
    comment: req.body.comment
  }).save((err, user) => {
    if (err) return res.json({ success: false, msg: err });
    console.log(chalk.white.bgGreen.underline.bold("Admin added"));
    return res.redirect("back");
  });

});

module.exports = router;
