//this is route manager for user panel
const express = require("express");
const router = express.Router();
//using path module to set static folder
var path = require("path");
//module loading to handle userpanel/api calls
const userapiroute = require("./api/api_user");
const userPostEdit = require("../tools/userfunctions/userPostEdit");

// handles main user panel route
router.get("/", function(req, res, next) {
    
  res.render("userpanel");
});

//handles new post route for user
router.get("/newpost", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("usernewpost");
});

//handles edit profile for self = > user
router.get("/editprofile", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("userprofileselfedit");
});

//
router.get("/editarticle", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));
    userPostEdit(req,res)
});




router.get("/deleteuserconfirm", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("userdeleteselfconfirm");
});

router.get("/myposts", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("userselfposts");
});

router.use("/api", userapiroute);

module.exports = router;
