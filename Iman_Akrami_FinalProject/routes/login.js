
let express = require('express');
let router = express.Router();
// experiment with storing global messages in module to access from multiple pages. 
const messages = require("../tools/messages");

/* handle login page*/
router.get('/', function(req, res, next) {
  
    if (messages.defaults.loginready.state) {
        messages.defaults.loginready.state = false
        res.render("login.ejs", { msg: messages.defaults.loginready.text });
      } else {
        res.render("login.ejs", { msg: messages.defaults.loginuser.text });
      }

});


module.exports = router;
