//this is general API file
const express = require("express");
const router = express.Router();
//using chalk, to display colored logs
const chalk = require("chalk");

const passport = require("passport");
const auth = require("../../tools/auth");
const ac = require("../../tools/routeprotector");

const adminAdder = require("../../tools/adminAdd");
const userAdder = require("../../tools/userAdd");
const singleArticle = require("../../tools/singleArticle");
const getAllComments = require("../../tools/getAllComments");
const getAllArticles = require("../../tools/getAllArticles");

/* handle general api requests*/
//no functionality here. maybe add some later?
router.all("/", function(req, res, next) {
  res.send("request to api route");
});

//login page api call. 
router.post("/login", passport.authenticate("local"), function(req, res, next) {
  if (req.user.role === "user") {
    return res.redirect("/userpanel");
  } else {
    return res.redirect("/adminpanel");
  }
});

//api request to log out for the current user. redirects to the 
router.post("/logout", function(req, res, next) {
  res.redirect("./logout");
});

// route to add admin through post. notice this has to be done using a tool like POSTMAN, no gui here.
router.put("/addadmin", function(req, res, next) {
  adminAdder(req, res, next);
});

//api route for adding registered user
router.post("/register", function(req, res, next) {
  userAdder(req, res, next);
});

// public route to get one single article using query strings
router.get("/getsinglearticle", function(req, res, next) {
  console.log(chalk.red.bgYellow.underline.bold("single article api call"));
  singleArticle(req, res);
});

// public route to get one article comments using query strings
router.get("/getallcomments?", function(req, res, next) {
  console.log(chalk.red.bgYellow.underline.bold("trying to get all comments"));
  getAllComments(req, res);
});

// publiic route to get all articles for the main page
router.get("/getallarticles", function(req, res, next) {
  console.log(
    chalk.red.bgYellow.underline.bold(
      "trying to get all articles for Public view"
    )
  );
  getAllArticles(req, res);
});


module.exports = router;
