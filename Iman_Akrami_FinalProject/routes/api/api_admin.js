//this is ADMIN API file => adminpanel/api
let express = require('express');
//using chalk, to display colored logs
const chalk = require("chalk");

let router = express.Router();

// using express file upload module. seemed more convinient that multer for now.
var fileUpload = require("express-fileupload");
//setting up the middleware
router.use(fileUpload());

// for the sake of modulariztion, i have seperate each function in its own file and put in the tools folder
const adminInfoGetter = require("../../tools/adminfunctions/adminInfoGetter");
const adminInfoGetterEdit = require("../../tools/adminfunctions/adminInfoGetterEdit");
const postAdminInfoEdit = require("../../tools/adminFunctions/postAdminInfoEdit");
const postAdminNewArticle = require("../../tools/adminfunctions/postAdminNewArticle");
const getAdminArticles = require("../../tools/adminfunctions/getAdminArticles");
const postAdminEditArticles = require("../../tools/adminfunctions/postAdminEditArticles");
const postAdminDeleteArticle = require("../../tools/adminfunctions/postAdminDeleteArticle");
const getAllArticles = require("../../tools/adminfunctions/getAllArticles");
const adminManageusers = require("../../tools/adminfunctions/adminManageusers");
const adminDeleteUser = require("../../tools/adminfunctions/adminDeleteUser");
const adminResetPassword = require("../../tools/adminfunctions/adminResetPassword");
const postadminUserEdit = require("../../tools/adminfunctions/postadminUserEdit");
const adminDeleteComment = require("../../tools/adminfunctions/adminDeleteComment");



/* handleadmin api requests*/
//main admin panel api get , #TODO maybe a functionality later?
router.get('/', function(req, res, next) {
  res.status(200).send('api admin accessed');
});
//get admin info for main panel
router.get("/getuserinfo", (req, res, next) => {
 adminInfoGetter(req, res, next);
});
//get admin info for edit profile page(admin)
router.get("/getadmininfoedit", (req, res, next) => {
  adminInfoGetterEdit(req, res, next);
});
//for the form in admin self profile edit page
router.post("/postadmininfoedit", function(req, res) {
  postAdminInfoEdit(req, res);
});

//for admin, adding a new article
router.post("/postadminnewarticle", function(req, res) {
  postAdminNewArticle(req, res);
});

//for agetting the list of admin articles
router.get("/getadminarticles", (req, res, next) => {
  getAdminArticles(req, res, next);
});

//for getting the list of all (admin and users) articles
router.get("/getallarticles", (req, res, next) => {
  getAllArticles(req, res, next);
});

//for admin, editing self articles, using query strings
router.post("/adminposteditarticle?", (req, res, next) => {
  postAdminEditArticles(req, res);
});

//for admin deleting users
router.get("/deletearticle", (req, res, next) => {
  postAdminDeleteArticle(req, res);
});

//for main user management page in admin panel
router.get("/manageusers", (req, res, next) => {
  adminManageusers(req, res, next);
 });
 
 //for admin, deleting a user
 router.get("/adminuserdelete?", (req, res, next) => {
  adminDeleteUser(req, res);
});

//for resetting user password to its mobile number
 router.get("/adminuserpasswordreset?", (req, res, next) => {
  adminResetPassword(req, res);
});

// #TODO for admin editing user
//  router.get("/adminedituser?", (req, res, next) => {
//   postadminUserEdit(req, res);
// });

//For admin deleting a comment
 router.get("/admindeletecomment?", (req, res, next) => {
  adminDeleteComment(req, res);
});


module.exports = router;
