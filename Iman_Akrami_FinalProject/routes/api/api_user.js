//this is general API file
let express = require("express");
let router = express.Router();
//using chalk, to display colored logs
const chalk = require("chalk");
//using path module to set static path
const path = require("path");
// using express file upload module. seemed more convinient that multer for now.
var fileUpload = require("express-fileupload");
//setting up the middleware
router.use(fileUpload());

// for the sake of modulariztion, i have seperate each function in its own file and put in the tools folder
const userInfoGetter = require("../../tools/userfunctions/userInfoGetter");
const userInfoGetterEdit = require("../../tools/userfunctions/userInfoGetterEdit");
const postUserInfoEdit = require("../../tools/userfunctions/postUserInfoEdit");
const postUserSelfDelete = require("../../tools/userfunctions/postUserSelfDelete");
const postUserNewArticle = require("../../tools/userfunctions/postUserNewArticle");
const getUserArticles = require("../../tools/userfunctions/getUserArticles");
const postUserEditArticles = require("../../tools/userfunctions/postUserEditArticles");
const postUserDeleteArticle = require("../../tools/userfunctions/postUserDeleteArticle");

/* handle general api requests*/
//for user main panel, #TODO maybe a functionality later?
router.get("/", (req, res, next) => {
  res.send("No need to see user api route...");
});

//api request to display user info for main page of userpanel
router.get("/getuserinfo", (req, res, next) => {
  userInfoGetter(req, res, next);
});

//api request to fill user self edit page
router.get("/getuserinfoedit", (req, res, next) => {
  userInfoGetterEdit(req, res, next);
});

//api request to apply user form for self profile edit
router.post("/postuserinfoedit", function(req, res) {
  postUserInfoEdit(req, res);
});

//api request for user to delete itself, #TODO make sure all corrosponding comments and articles are also deleted.
router.post("/userselfdelete", function(req, res) {
  router.use(express.static(path.join(__dirname + "../../")));

  postUserSelfDelete(req, res);
});

//api request for user for adding new article = > user
router.post("/postusernewarticle", function(req, res) {
  postUserNewArticle(req, res);
});

// api request to get user articles to populate them on the "myposts" page in user panel
router.get("/getuserarticles", (req, res, next) => {
  getUserArticles(req, res, next);
});

// api request for user editting self articles.
router.post("/userposteditarticle?", (req, res, next) => {
  postUserEditArticles(req, res);
});

//api request for user deleteing one article #TODO make sure comments are also deleted
router.get("/deletearticle", (req, res, next) => {
  postUserDeleteArticle(req, res);
});

module.exports = router;
