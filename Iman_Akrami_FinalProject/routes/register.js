
let express = require("express");
let router = express.Router();
const messages = require("../tools/messages");

/* handle register route also using experimnt with global module: messages that i created*/
router.get("/", function(req, res, next) {
  
  
  if (messages.defaults.registerform.state) {
    messages.defaults.registerform.state = false
    res.render("register.ejs", { msg: messages.errors.alreadyexists.text });
  } else {
    res.render("register.ejs", { msg: messages.defaults.registerform.text });
  }
});

module.exports = router;
