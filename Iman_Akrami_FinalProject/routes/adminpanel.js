//this is route controller for admin panel
const express = require("express");
const router = express.Router();
var path = require("path");

//getting admin api route controller
const adminapi = require("./api/api_admin");
const adminPostEdit = require("../tools/adminfunctions/adminPostEdit");
// const adminUserEditGet = require("../tools/adminfunctions/adminUserEditGet");

// handles main admin panel route
router.get("/", function(req, res, next) {
  
  res.render("adminpanel");
});

//handles new post page
router.get("/newpost", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("adminnewpost");
});

//handles manage users route
router.get("/manageusers", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("adminusermanage");
});

//handles admin profile (self) edit route
router.get("/editprofile", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("adminprofileselfedit");
});

//handles admin my posts route
router.get("/myposts", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("adminselfposts");
});

//handles display all posts route
router.get("/allposts", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));

  res.render("adminallposts");
});

//handles admin post edit route
router.get("/editarticle", function(req, res, next) {
  router.use(express.static(path.join(__dirname + "/../public")));
    adminPostEdit(req,res)
});

// handles admin edit users route #TODO add this functionality later
// router.get("/adminedituser?", function(req, res, next) {
//   router.use(express.static(path.join(__dirname + "/../public")));
//   res.render("adminuseredit");
//     // adminUserEditGet(req,res)
// });


//setting up middleware to route api calls
router.use("/api", adminapi);

module.exports = router;
