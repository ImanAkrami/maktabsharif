
let express = require('express');
let router = express.Router();
const chalk = require('chalk');
/* handle logout route */
router.get('/', function(req, res, next) {
    req.logout();
    console.log(chalk.white.bgGreen.underline.bold('Logged out'));
    res.render('../views/logout.ejs')
});

module.exports = router;
