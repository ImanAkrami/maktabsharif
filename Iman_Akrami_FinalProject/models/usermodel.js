const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstname:{
        type: String,
        required: true,
        trim: true
    },
    lastname:{
        type: String,
        required: true,
        trim:true
    },
    //making the username lowercas so id doesn't matter
    username:{
        type: String,
        unique: true,
        required: true,
        trim: true,
        lowercase:true
    },
    // #TODO , have to use bycrypt to hash the password.
    password:{
        type: String,
        required:true,
        maxlength:20,
        minlength:6,
        trim: true
    },
    gender:{
        type: String,
        required: true,
        enum:['male','female']

    },
    role:{
        type: String,
        default:'user',
        enum:['user','admin']
    },
    mobile:{
        type: String,
        required: true,
        trim: true
    },
    //this will point to the default picture , in case user is new, or he/she has not assigned one yet
    avatar:{
        type: String,
        default:"images/defaultprofile.png"
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    update_at: {
        type: Date,
        required: true,
        default: Date.now
    }
})

const User = mongoose.model('UserSchema',UserSchema)
module.exports = User;