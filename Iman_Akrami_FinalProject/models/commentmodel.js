const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const commentSchema = new Schema({
    //referring to the Author id of the comment 
    author_ID : {
        type:Schema.Types.ObjectId, 
        ref:'UserSchema',
        required:true
    },
    //referring to the article id in which comment is written
    article_ID:{
        type:Schema.Types.ObjectId, 
        ref:'articleSchema',
        required:true
    },
    commenttitle:{
        type: String,
    },
    comment:{
        type: String,
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    }
})

const Comments = mongoose.model('commentSchema',commentSchema)
module.exports = Comments;
