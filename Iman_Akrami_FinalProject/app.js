//***************************************************/
//*********Loading The Required modules *******//
//*************************************************** */
// using passport + season for authentication and authorization
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const session = require("express-session");
const passport = require("passport");

//***************************************************/
//*********Loading The Required Custom modules *******//
//*************************************************** */

//Authentication strategy
const auth = require("./tools/auth");
// Route controller/protector for user and admin routes and sub routes
const ac = require("./tools/routeprotector");
// mongodb initilizer which recieves an argument, url of the server
const mongoInit = require("./tools/mongoInit");
// route for public
let indexRouter = require("./routes/index");
// route for public api
let apiRouter = require("./routes/api/api");
//route for  registeration
let registerRouter = require("./routes/register");
//route for login
let loginRouter = require("./routes/login");
//Route for logging out
let logoutRouter = require("./routes/logout");
//Route for admin panel
let adminPanelRouter = require("./routes/adminpanel");
//Route for user panel
let userPanelRouter = require("./routes/userpanel");
//Route for public displaying of single article
let singlearticleRoute = require("./routes/singlearticle");


//***************************************************/
//*********Setting up app and middlewares *******//
//*************************************************** */
//defining the main app
let app = express();

//calling mongo initilizer on the given server url
mongoInit("mongodb://localhost:27017/TinyBlog");

// setting up to use season middleware
app.use(
  session({
    saveUninitialized: true,
    secret: "1!2@3$qQwWeE1qaz2wsx3edc4rfv",
    resave: true,
    cookie: {
      maxAge: 600000
    }
  })
);

//setting up to use passport and season middle ware
app.use(passport.initialize());
app.use(passport.session());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//settingup logger and express
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//Setting up express static path to "public" folder
app.use(express.static(path.join(__dirname, "public")));

// Initilizing middlewares for routes
app.use("/", indexRouter);
app.use("/register", registerRouter);
app.use("/login", loginRouter);
app.use("/logout", logoutRouter);
app.use("/api", apiRouter);
app.use("/article", auth.isLogin, singlearticleRoute);
//initilizing route protectors
app.use(
  "/adminpanel",
  auth.isLogin,
  ac.routeController(["admin"]),
  adminPanelRouter
);
app.use(
  "/userpanel",
  auth.isLogin,
  ac.routeController(["user"]),
  userPanelRouter
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
