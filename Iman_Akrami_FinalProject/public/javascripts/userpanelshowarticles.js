//this function gets and populates all of the user articles.
function userallarticles() {
    let currentArticle
    let imgLink 
    let shortText
    $.ajax({
        
        url: '/userpanel/api/getuserarticles',
        type: 'GET',
            
         success: function(data) {
            
             for (const key in data) {
                 currentArticle = data[key]
                 shortText = currentArticle.articleContent.slice(0,100)
                 imgLink = currentArticle.articleImage.slice(7)
    
                 $('#article-container').append(`<div id="${currentArticle._id}" class="col-sm-6 col-md-4 item">
                 <img class="img-fluid" src="../${imgLink}" />
                 <h3 id="article-title" class="name">${currentArticle.articleTitle}</h3>
                 <p id="article-shorttext" class="description">
                 ${shortText}
                 </p>
                 <div class="d-flex justify-content-between">
                   <a class="action" href="/article/${currentArticle._id}"><i class="fa fa-align-justify"></i></a
                   ><a class="action" href="/userpanel/editarticle?id=${currentArticle._id}"><i class="fa fa-edit"></i></a
                   ><a class="action" href="/userpanel/api/deletearticle?id=${currentArticle._id}"><i class="fa fa-trash"></i></a>
                 </div>
               </div>`)
                 
             }

         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    userallarticles()

});