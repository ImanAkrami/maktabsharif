//This function gets the information to populate main userpanel page.
function userinfogettr() {
   
    $.ajax({
        
        url: '/userpanel/api/getuserinfo',
        type: 'GET',
        
         success: function(data) {

            console.log(data.users);
            
            $("#welcometitle").append(data.users.firstname)
            $("#nameinfo").append(data.users.firstname)
            $("#lastnameinfo").append(data.users.lastname)
            $("#usernameinfo").append(data.users.username)
            $("#genderinfo").append(data.users.gender)
            $("#mobileinfo").append(data.users.mobile)
            if (!data.users.avatar=="images/defaultprofile.png") {
                $("#avatar").attr('src',data.users.avatar)
            }
            
            
         }
    });
}
$(document).ready(function () {
    userinfogettr()
});