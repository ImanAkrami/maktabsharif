// This function gets the required information from admin panel api route, and displays them on the admin info panel
function userinfogettr() {
   
    $.ajax({
        
        url: '/adminpanel/api/getuserinfo',
        type: 'GET',
        
         success: function(data) {

            console.log(data.users);
            
            $("#welcometitle").append(data.users.firstname)
            $("#nameinfo").append(data.users.firstname)
            $("#lastnameinfo").append(data.users.lastname)
            $("#usernameinfo").append(data.users.username)
            $("#genderinfo").append(data.users.gender)
            $("#mobileinfo").append(data.users.mobile)
            if (!data.users.avatar=="") {
                $("#avatar").attr('src',data.users.avatar)
            }
            else{
                $("#avatar").attr('src',data.users.avatar)
            }
            
         }
    });
}
$(document).ready(function () {
    userinfogettr()
});