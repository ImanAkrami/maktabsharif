//this function, gets gets user information from the specified route, and loops through the returned result to display the user list.
function userallarticles() {
    let currentUser
    let id
    let firstname
    let lastname 
    let username
    let gender
    let mobile
    let avatar

    $.ajax({
        
        url: '/adminpanel/api/manageusers',
        type: 'GET',
            
         success: function(data) {
            
             for (const key in data) {
                 currentUser = data[key]                 
                 id = currentUser._id
                 firstname = currentUser.firstname
                 lastname = currentUser.lastname
                 username = currentUser.username
                 gender = currentUser.gender
                 mobile = currentUser.mobile
                 avatar = currentUser.avatar
                 $('#user-list-container').append(`<tr id="${id}">
                 <td >${firstname}</td>
                 <td>${lastname}</td>
                 <td>${username}</td>
                 <td>${gender}</td>
                 <td>${mobile}</td>
                 <td><img height="50px" width="auto" src="../${avatar}"></img></td>
                 <td class="d-flex justify-content-around"><a href="#"><i class="fa fa-edit"></i></a><a href="api/adminuserpasswordreset?id=${id}"><i class="fa fa-undo"></i></a><a href="api/adminuserdelete?id=${id}"><i class="fa fa-ban"></i></a></td>
             </tr>`)
                 
             }

         
            
         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    userallarticles()
    // postinfouseredit()
});