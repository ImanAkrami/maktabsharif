//this function sens a GET request to the admin panel api, to pre-fill the edit admin form.
function userinfogetteredit() {
   
    $.ajax({
        
        url: '/adminpanel/api/getadmininfoedit',
        type: 'GET',
            
         success: function(data) {
             console.log(data.users);

            $("#firstname").val(data.users.firstname)
            $("#lastname").val(data.users.lastname)
            $("#username").val(data.users.username)
            $("#mobile").val(data.users.mobile)
            $("#password").val(data.users.password)
            $("#gender").val(data.users.gender).change();
            
         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    userinfogetteredit()
    // postinfouseredit()
});