//This function gets user information and populates the edit profile form for user route.
function userinfogetteredit() {
   
    $.ajax({
        
        url: '/userpanel/api/getuserinfoedit',
        type: 'GET',
            
         success: function(data) {

            $("#firstname").val(data.users.firstname)
            $("#lastname").val(data.users.lastname)
            $("#username").val(data.users.username)
            $("#mobile").val(data.users.mobile)
            $("#password").val(data.users.password)
            $("#gender").val(data.users.gender).change();
            
         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    userinfogetteredit()
    // postinfouseredit()
});