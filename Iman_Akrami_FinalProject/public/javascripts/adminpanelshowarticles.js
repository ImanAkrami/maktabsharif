// ajax request to get and display all admin articles.
function userallarticles() {
    let currentArticle
    let imgLink 
    let shortText
    $.ajax({
        
        url: '/adminpanel/api/getadminarticles',
        type: 'GET',
            
         success: function(data) {
            
             for (const key in data) {
                 currentArticle = data[key]
                //using slice to make a short version of the text (first 100 letters, seems fair)
                // #TODO make a db field to hold this part, so the response size is minimzed?
                 shortText = currentArticle.articleContent.slice(0,100)
                 //using slice to remove the additional charachtes saved in the db image path.
                 imgLink = currentArticle.articleImage.slice(7)
    
                 $('#article-container').append(`<div id="${currentArticle._id}" class="col-sm-6 col-md-4 item">
                 <img class="img-fluid" src="../${imgLink}" />
                 <h3 id="article-title" class="name">${currentArticle.articleTitle}</h3>
                 <p id="article-shorttext" class="description">
                 ${shortText}
                 </p>
                 <div class="d-flex justify-content-between">
                   <a class="action" href="/article/${currentArticle._id}"><i class="fa fa-align-justify"></i></a
                   ><a class="action" href="/adminpanel/editarticle?id=${currentArticle._id}"><i class="fa fa-edit"></i></a
                   ><a class="action" href="/adminpanel/api/deletearticle?id=${currentArticle._id}"><i class="fa fa-trash"></i></a>
                 </div>
               </div>`)
                 
             }

         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    userallarticles()
    // postinfouseredit()
});