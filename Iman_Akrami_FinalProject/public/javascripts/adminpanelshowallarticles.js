function userallarticles() {
    let currentArticle
    let imgLink 
    let shortText
    let authorName
    let authodID
    $.ajax({
        
        url: '/adminpanel/api/getallarticles',
        type: 'GET',
            
         success: function(data) {
            
             for (const key in data) {
                 currentArticle = data[key]
                 shortText = currentArticle.articleContent.slice(0,100)
                 imgLink = currentArticle.articleImage.slice(7)
                 authorName = currentArticle.author_ID.firstname +" "+ currentArticle.author_ID.lastname
                 authodID = currentArticle.author_ID._id
                 $('#article-container').append(`<div id="${currentArticle._id}" class="col-sm-6 col-md-4 item">
                 <img class="img-fluid" src="../${imgLink}" />
                 <h3 id="article-title" class="name">${currentArticle.articleTitle}</h3>
                 <a href="userposts?${authodID}"><span style="font-size:12px" class="text-monospace">by ${authorName}</span></a>
                 <p id="article-shorttext" class="description">
                 ${shortText}
                 </p>
                 <div class="d-flex justify-content-between">
                   <a class="action" href="/article/${currentArticle._id}"><i class="fa fa-align-justify"></i></a
                   ><a class="action" href="/adminpanel/editarticle?id=${currentArticle._id}"><i class="fa fa-edit"></i></a
                   ><a class="action" href="/adminpanel/api/deletearticle?id=${currentArticle._id}"><i class="fa fa-trash"></i></a>
                 </div>
               </div>`)
                 
             }

            // $("#firstname").val(data.users.firstname)
            // $("#lastname").val(data.users.lastname)
            // $("#username").val(data.users.username)
            // $("#mobile").val(data.users.mobile)
            // $("#password").val(data.users.password)
            // $("#gender").val(data.users.gender).change();
            
         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    userallarticles()
    // postinfouseredit()
});