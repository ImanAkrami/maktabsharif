//this function gets single article (for public route) and populates the bootstrap column, with a for loop.
function publicallarticles() {
    let currentArticle
    let imgLink 
    let shortText
    $.ajax({
        
        url: 'api/getallarticles',
        type: 'GET',
            
         success: function(data) {
            
             for (const key in data) {
                 currentArticle = data[key]
                 shortText = currentArticle.articleContent.slice(0,100)
                 imgLink = currentArticle.articleImage.slice(7)
    
                 $('#article-container').append(`<div id="${currentArticle._id}" class="col-sm-6 col-md-4 item">
                 <a class="action" href="/article/${currentArticle._id}"><img class="img-fluid" src="../${imgLink}" /></a
                   >
                 
                 <h3 id="article-title" class="name">${currentArticle.articleTitle}</h3>
                 <p id="article-shorttext" class="description">
                 ${shortText}
                 </p>
                 <div class="d-flex justify-content-center">
                   <a class="action" href="/article/${currentArticle._id}"><i class="fa fa-align-justify"></i></a
                   >
                 </div>
               </div>`)
                 
             }
            
         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    publicallarticles()
    // postinfouseredit()
});