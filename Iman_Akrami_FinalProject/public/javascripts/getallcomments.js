//This function gets all comment from a specific article, using query strings
//notice i had to slice the first few charachters to remove extra relative path charachters.
function getallcomments() {
    let urlToAjax = '/api/getallcomments?id=' + window.location.pathname.slice(9)
    let author
    let commentid
    let commenttext
    let commenttitle
    $.ajax({
        
        url: urlToAjax,
        type: 'GET',
            
         success: function(data) {

            
             for (const key in data) {
                 comment = data[key]
                 author = comment.author_ID.firstname + " " + comment.author_ID.lastname
                 commenttitle = comment.commenttitle
                 commenttext = comment.comment
                 commentid=comment._id
                 $('#comment-container').append(`<h4>${commenttitle}</h4>
                 <strong><p>${author}&nbsp;</p></strong>
                 <p>${commenttext}&nbsp;</p>
                 <a href="/adminpanel/api/admindeletecomment?id=${commentid}"><i class="fa fa-ban"></i></a>`)
                 
             }

            
         }
       
    });
}


$(document).ready(function () {
    console.log('Connected to the browser');
    getallcomments()
});