// server.js
// load the things we need
var express = require("express");
var app = express();
app.use(express.static("views/"));
// set the view engine to ejs
app.set("view engine", "ejs");

// use res.render to load up an ejs view file

// index page
app.get("/", function(req, res) {
  res.render("pages/index");
});

// about page
app.get("/about", function(req, res) {
  res.render("pages/about");
});

// about page
app.get("/contact", function(req, res) {
    res.render("pages/contact");
  });



  const cardsData= require(__dirname + '/views/data/cards-data.json')

  app.get('/cd', function (req, res) {
    res.header("Content-Type",'application/json');
    res.send(JSON.stringify(cardsData));
  })

  
  
  app.get("/:id", function(req, res) {
    
    
    res.render("pages/card", {
        title: cardsData[req.params.id[4]-1]['title'],
        desc: cardsData[req.params.id[4]-1]['description']
    });
  });
  
  

app.listen(8089);
console.log("8089 is the magic port");
console.log()