var express = require("express");
var router = express.Router();
router.use(express.static('public'))


// index page 
router.get('/', function(req, res) {
  res.render('public/index');
});

// router.get('/',function(req,res) {
//     res.sendFile('/public/index.html', { root: __dirname });
//   });
  router.get('/about',function(req,res) {
   res.sendFile('/public/about.html', { root: __dirname });
 });
 router.get('/contact',function(req,res) {
   res.sendFile('/public/contact.html', { root: __dirname });
 });
 router.get('/c1',function(req,res) {
   res.sendFile('/public/cards/c1.html', { root: __dirname });
  });

 router.get('*', function(req, res){
    res.send('Sorry, this is an invalid URL.');
 });
//export this router to use in our index.js
module.exports = router;
