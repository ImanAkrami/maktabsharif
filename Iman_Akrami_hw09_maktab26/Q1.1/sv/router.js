var express = require("express");
var router = express.Router();
router.use(express.static('public'))

router.get('/',function(req,res) {
    res.sendFile('/public/index.html', { root: __dirname });
  });
  router.get('/about',function(req,res) {
   res.sendFile('/public/about.html', { root: __dirname });
 });
 router.get('/contact',function(req,res) {
   res.sendFile('/public/contact.html', { root: __dirname });
 });
 router.get('/c1',function(req,res) {
   res.sendFile('/public/cards/c1.html', { root: __dirname });
 });
 router.get('/c2',function(req,res) {
   res.sendFile('/public/cards/c2.html', { root: __dirname });
 });
 router.get('/c3',function(req,res) {
   res.sendFile('/public/cards/c3.html', { root: __dirname });
 });
 router.get('/c4',function(req,res) {
   res.sendFile('/public/cards/c4.html', { root: __dirname });
 });
 router.get('/c5',function(req,res) {
   res.sendFile('/public/cards/c5.html', { root: __dirname });
 });
 router.get('/c6',function(req,res) {
   res.sendFile('/public/cards/c6.html', { root: __dirname });
 });

 router.get('*', function(req, res){
    res.send('Sorry, this is an invalid URL.');
 });
//export this router to use in our index.js
module.exports = router;
