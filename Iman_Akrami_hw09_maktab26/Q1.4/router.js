var express = require("express");
var router = express.Router();
var bodyParser = require("body-parser");

router.use(bodyParser.json()); // support json encoded bodies
router.use(bodyParser.urlencoded({ extended: true })); // support

router.use(express.static("public"));

const cardsData = require(__dirname + "/public/data/up.json");

router.get("/", function(req, res) {
  res.sendFile("/public/pages/index.html", { root: __dirname });
});

// POST http://localhost:8080/api/users
// parameters sent with
router.post("/", function(req, res) {
  console.log(req.body);

  for (let user = 0; user < cardsData.length; user++) {
    if (cardsData[user]["userName"] == req.body["user"] && cardsData[user]["hash"].toLowerCase() == req.body["hash"].toLowerCase() ) {
      res.status(200).send({ result: "bothCorrect" });
      

      break;
    } 
    else if (cardsData[user]["userName"] == req.body["user"] && cardsData[user]["hash"].toLowerCase() != req.body["hash"].toLowerCase()) {
      res.status(200).send({ result: "passwordIncorrect" });
      

      break;
    }
    else if (cardsData[user]["userName"] != req.body["user"] && cardsData[user]["hash"].toLowerCase() != req.body["hash"].toLowerCase()) {
      res.status(200).send({ result: "bothIncorrect" });
      

      break;
    }
    else{
      res.status(200).send({ result: "otherError" });
      

      break;
    }
  }


});

router.get("/up", function(req, res) {
  res.header("Content-Type", "application/json");
  res.send(JSON.stringify(cardsData));
});

router.get("*", function(req, res) {
  res.send("Sorry, this is an invalid URL.");
});
//export this router to use in our index.js
module.exports = router;
