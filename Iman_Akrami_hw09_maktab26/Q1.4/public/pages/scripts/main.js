console.log("connected");


$("#error-box").hide();
$(document).ready(function() {
  $("#submit-button").click(function(event) {
    event.preventDefault();

    let formData = $("form#myForm").serializeArray();

    formObject = JSON.stringify({
      user: formData[0]["value"],
      hash: forge_sha256(formData[1]["value"])
    });
    console.log(formObject);
    
    $.ajax({
      url: "/",
      type: "post",
      dataType: "json",
      contentType: "application/json; charset=utf-8",
      data: formObject,
      success: function(data) {
        
        if ((data.result == "passwordIncorrect")) {
          console.log(data.result);
          $("#error-box").fadeIn();
          $("#error-box").text('پسورد اشتباه است');
          
        } else if ((data.result == "bothCorrect")) {
          console.log(data.result);
          $("#error-box").fadeIn();
          $("#error-box").text('هر دو صحیح هستند');
        } else if ((data.result == "bothIncorrect")) {
          console.log(data.result);
          $("#error-box").fadeIn();
          $("#error-box").text('نام کاربری و پسورد اشتباه هستند');
        }
        else {
          $("#error-box").fadeIn();
          $("#error-box").text('خطای نامشخص');
        }
      }
    });
  });
});
