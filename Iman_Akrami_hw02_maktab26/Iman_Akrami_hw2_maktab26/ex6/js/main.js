console.log('Connected to browser')

// here's  just a sample input array to test the function on

let inputArray = [1,2,'Hi',true,[1,2],[2,3],[4,5],alert,null,
null,null,null,undefined,undefined,]

// defining each type identifier functions. it will make it easier 
// to implement them in the main one:

// Returns if a value is a string
function isString (value) {
    return typeof value === 'string' || value instanceof String;
    }
// Returns if a value is really a number
function isNumber (value) {
    return typeof value === 'number' && isFinite(value);
    }

// Returns if a value is an array
// ES5 actually has a method for this , but for the sake of a unfied answer
//to the questions, i will not use this:
// Array.isArray(value);
function isArray (value) {
    return value && typeof value === 'object' && value.constructor === Array;
    }
    
// Returns if a value is a function
function isFunction (value) {
    return typeof value === 'function';
    }    

// Returns if a value is an object
function isObject (value) {
    return value && typeof value === 'object' && value.constructor === Object;
    }

// Returns if a value is null
function isNull (value) {
    return value === null;
    }
    
// Returns if a value is undefined
    function isUndefined (value) {
    return typeof value === 'undefined';
    }

// Returns if a value is a boolean
function isBoolean (value) {
    return typeof value === 'boolean';
    }

// Returns if a Symbol. this is a new data type that we have not been really
//discussing in Maktab
function isSymbol (value) {
    return typeof value === 'symbol';
    }


// now, i will be defining the main function
function arrayElementTypeChecker(arr) {
    //Defining the variables to hold numbers for each type
    let numOfStrings=0,numOfNumbers=0,numOfArrays=0;
    let numOfFunctions=0,numOfObjects=0,numOFNulls=0;
    let numOfUndefineds=0,numOfBooleans=0,numOfSymbols=0,numOfOthers=0;
//Looping through the array to detemine the types
    for (let index = 0; index < arr.length; index++) {
        if (isString(arr[index])) {
            numOfStrings++

        } else if(isNumber(arr[index])) {
            numOfNumbers++
        }
        else if(isArray(arr[index])) {
            numOfArrays++
        }
        else if(isFunction(arr[index])) {
            numOfFunctions++
        }
        else if(isObject(arr[index])) {
            numOfObjects++
        }
        else if(isNull(arr[index])) {
            numOFNulls++
        }
        else if(isUndefined(arr[index])) {
            numOfUndefineds++
        }
        else if(isBoolean(arr[index])) {
            numOfBooleans++
        }
        else{
            numOfOthers++
        }
    }

// now printing out the number of each type:

    console.log('Number of strings: '+ numOfStrings + "\n"
    +'Number of numbers: '+ numOfNumbers + "\n"
    +'Number of arrays: '+ numOfArrays + "\n"
    +'Number of functions: '+ numOfFunctions + "\n"
    +'Number of objects: '+ numOfObjects + "\n"
    +'Number of nulls: '+ numOFNulls + "\n"
    +'Number of undefineds: '+ numOfUndefineds + "\n"
    +'Number of booleans: '+ numOfBooleans + "\n"
    +'Number of symbols: '+ numOfSymbols + "\n"
    +'Number of others: '+ numOfOthers + "\n" )

}
arrayElementTypeChecker(inputArray)