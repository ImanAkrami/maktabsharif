console.log('Connected to browser')




let arr1 = [3,3,'a',true, 3, 'b',9] 
let arr2 = [3,3,'b',true,99,6,'z',[3]]

function CommonElementFinder(arr1,arr2) {
    var commonValues = [];
    var i, j;
    var arr1Length = arr1.length;
    var arr2Length = arr2.length;
    //making array elements unique by converting them to Set and back
    arr1 =  Array.from(new Set(arr1));
    arr2 =  Array.from(new Set(arr2));

for (i = 0; i < arr1Length; i++) {
    for (j = 0; j < arr2Length; j++) {
        if (arr1[i] === arr2[j]) {
            commonValues.push(arr1[i]);
        }
    }
}
return commonValues
}
console.log (CommonElementFinder(arr1,arr2))
