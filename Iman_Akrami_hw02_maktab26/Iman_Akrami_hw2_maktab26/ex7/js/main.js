console.log('Connected to browser')


 function depthFinder(object){
    let level = 1;
    let key;
    for(key in object) {
        if (!object.hasOwnProperty(key)) continue;

        if(typeof object[key] == 'object'){
            let depth = depthFinder(object[key]) + 1;
            level = Math.max(depth, level);
        }
    }
    return level;
}

console.log(depthFinder([1,[3,3,3,3],2,[1,3,[1,[1]]]]))