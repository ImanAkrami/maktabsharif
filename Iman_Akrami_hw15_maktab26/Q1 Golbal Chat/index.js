const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);


app.use(express.static(__dirname + '/resources'));


app.get('/', function(req, res) {
    res.render('index.ejs');
});

io.sockets.on('connection', function(socket) {
    //For when user connects
    socket.on('username', function(username) {
        socket.username = username;
        //checking if username is not empty, null, or undefined
        if (socket.username!=''&&socket.username!=null&&socket.username!=undefined) {
        io.emit('is_online', '<span class="online_icon">Online</span> <i>' + socket.username + ' has joined the chat!</i>');
        }
    });
    //For when user disconnects
    socket.on('disconnect', function(username) {
        socket.username = username;
        //checking if user name is not empty, null, or undefined
        if (socket.username!=''&&socket.username!=null&&socket.username!=undefined) {
        io.emit('is_online', '<span class="offline">Offline</span> <i>' + socket.username + ' has left the chat</i>');
        }
    });
    //For when user sends a message
    socket.on('chat_message', function(message) {
        //checking if message is not empty, null, or undefined
        if (message!=''&&message!=null&&message!=undefined) {
        io.emit('chat_message', '<strong>' + socket.username + '</strong>: ' + message );
        }
    });

});

const server = http.listen(3000, function() {
    console.log('listening port : 3000');
});