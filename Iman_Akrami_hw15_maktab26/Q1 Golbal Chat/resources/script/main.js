console.log("js connected");
// ask username
let username = prompt("Please enter a nickname");
const socket = io.connect("http://localhost:3000");

if (username != "" && username != null && username != undefined) {
    socket.emit("username", username);
    // submit text message without reload/refresh the page
    $("form").submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit("chat_message", $("#txt").val());
        $("#txt").val("");
        return false;
    });

    // append the chat text message
    socket.on("chat_message", function (msg) {
        $("#chat-frame").append($("<li>").html(msg));
    });

    // append text if someone is online
    socket.on("is_online", function (username) {
        $("#chat-frame").append($("<li>").html(username));
    });
} else {
    $("#send").attr("disabled", true);
}