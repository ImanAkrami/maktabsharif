// =======================Adding functionality==========================
$(document).ready(function () {
   // =======================DISPLAY ADMIN INFO==========================
   //this ajax request gets the info from admin/editprofile route, using
   // GET method
    $("#showInfo").click(function () {
        //Clearing the previous data in the row
        $("#trRow").find("td").remove();
        //Making the request and adding data
        $.ajax({    
            url: 'admin/editprofile',
            type: 'GET',
            success: function (result) {
                for (let key in result.users) {
                    // for keys in the object of result.users OTHER than avatar:
                    if('avatar'!=key){
                        // append them to the #trRow and adding ID according to each key
                        $("#trRow").append("<td id='"+key+"'>"+result.users[key] +"</td>");
                    }else{
                        //if the key is equal to profile picture (avatar), after creating a td with the given key, sets the src to the file name
                        $("#trRow").append("<td id='"+key+"'><img src='../uploads/"+result.users[key]+"' style='max-width: 100px;'></td>");
                    }
                 }
                 //After the for loop, adds the EDIT button  with id="edit" to the AFTER of 5th td
                $('td:eq(5)').after("<td><button class='btn btn-info btn-xs btn-edit' id='edit' type='submit'>Edit</button></td>");
            },
            error: function (err) {
                //logging out if there is any error?
                console.log(err);
            }   
        });
    });

// =======================EDIT button==========================
    $("body").on("click", "#edit", function (e) { 
    e.preventDefault();
    //getting the value of the current (none avatar) fileds
    var firstName = $("#firstName").text();
    var lastName = $("#lastName").text();
    var password = $("#password").text();
    var role = $("#role").text();
    var mobile = $("#mobile").text();

    // making the fileds (selecting by ID) editable by converting them to inputs and using the values from above
    $("#firstName").html('<input name="firstName" value="' + firstName + '">');
    $("#lastName").html('<input name="lastName" value="' + lastName + '">');
    $("#password").html('<input name="password" value="' + password + '">');
    $("#role").html('<input name="role" value="' + role + '">');
    $("#mobile").html('<input name="mobile" value="' + mobile + '">');

    // displaying OK/CANCEL buttons with id UPDATE and cancelAdmin
    $(this).parents("tr").find("td:last").prepend("<button class='btn btn-info btn-xs btn-update' id='update'>OK</button><button class='btn btn-warning btn-xs btn-cancel' id='cancelAdmin'>Cancel</button>");
    //Hiding the EDIT button
    $(this).hide();
    console.log("ADMIN: admin edit => Clicked the EDIT button");

    });


// =======================UPDATE/OK button==========================
//will use js in combinition with jquery to FETCH
    $("body").on("click", "#update", function () {
        fetch('admin/editUserAdmin', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstname: $("input[name = firstName]").val(),
                lastname: $("input[name = lastName]").val(),
                password: $("input[name = password]").val(),
                role: $("input[name = role]").val(),
                mobile: $("input[name = mobile]").val(),
            })
            
        });
    
    
        }); 
    
    
    //will return the fileds to their original state
    $("body").on("click", "#update", function (e) { 
        e.preventDefault();
        //displaying EDIT button again
        $(".btn-edit").show();
        $("#firstName").text($("input[name = firstName]").val());
        $("#lastName").text($("input[name = lastName]").val());
        $("#password").text($("input[name = password]").val());
        $("#role").text($("input[name = role]").val());
        $("#mobile").text($("input[name = mobile]").val());
        
        // removing the OK/UPDATE and CANCEL button
        $("#update").remove();
        $("#cancelAdmin").remove();       
        console.log("ADMIN: admin update => UPDATED successfully");
    });
    


// ======================CANCEL button==========================
//will return the fileds to their original state
    $("body").on("click", "#cancelAdmin", function (e) { 
        e.preventDefault();
        //displaying EDIT button again
        $(".btn-edit").show();
        $("#firstName").text($("input[name = firstName]").val());
        $("#lastName").text($("input[name = lastName]").val());
        $("#password").text($("input[name = password]").val());
        $("#role").text($("input[name = role]").val());
        $("#mobile").text($("input[name = mobile]").val());
        // removing the OK/UPDATE and CANCEL button
        $("#update").remove();
        $("#cancelAdmin").remove();        
        console.log("ADMIN: admin edit => CANCELED Successfully");
    });
    




// =======================USER CRUD FUNCTIONALITY SECTION==========================
    //GET request to get all users PAGE
    $( "#editUsers" ).click(function() {
        $.ajax({
            url: 'admin/getAllUsers',
            type: 'GET',
            success: function (result) {
            },
            error: function (err) {
                console.log(err);
            }
        });
        console.log("ADMIN: user edit => GET request made ");
      });
      

    // GET request to make a query to get all users in db
    $("#showUsers").click(function () {
        console.log("ADMIN: user edit => GET request made to db ");
        $("#tbodyUsers").find("tr").remove();
        $.ajax({    
            url: './showGetAllUsers',
            type: 'GET',
            success: function (result) {
                for (let key in result.users) {
                    $("#tbodyUsers").append("<tr id='tr_"+key+"'></tr>");
                    for (let keys in result.users[key] ) {
                        $("#tr_"+key+"").append("<td id='tr_"+key+"_"+keys+"' data-"+keys+"='"+result.users[key][keys]+"'>"+result.users[key][keys] +"</td>");
                    }
                    $('#tr_'+key+'').append("<td><button class='btn btn-info btn-xs btn-edit' id='editUser' type='submit'>Edit</button> <button class='btn btn-danger btn-xs btn-delete' id='deleteUser' type='submit'>Delete</button> <button class='btn btn-danger btn-xs btn-reset' id='resetPassword' type='submit'>Reset Pass</button></td>");
                 } 
            },
            error: function (err) {
                console.log(err);
            } 
        });
    });
    //saving the current values in variables for later reference
    $("body").on("click", "#editUser", function (e) { 
    e.preventDefault();
    var firstName = $(this).parents('tr').find("td:eq(0)").text();
    var lastName = $(this).parents('tr').find("td:eq(1)").text();
    var mobile = $(this).parents('tr').find("td:eq(2)").text();
    var role = $(this).parents('tr').find("td:eq(3)").text();

        
    //convering td to inputs for user to edit
    $(this).parents('tr').find("td:eq(0)").html('<input name="firstName" value="' + firstName + '">');
    $(this).parents('tr').find("td:eq(1)").html('<input name="lastName" value="' + lastName + '">');
    $(this).parents('tr').find("td:eq(2)").html('<input name="mobile" value="' + mobile + '">');
    //hiding the delete user field
    $(this).parents('tr').find("#deleteUser").hide();
    //prepend update/confitm button
    $(this).parents("tr").find("td:last").prepend("<button class='btn btn-info btn-xs btn-update' id='updateUser'>Update</button><button class='btn btn-warning btn-xs btn-cancel' id='cancelUser'>Cancel</button>");
    $(this).hide();
    console.log("ADMIN: user edit => edit user button ");


    });

    // when user clicks update to confirm the changes, save values as
    //variables by thei input name
    $("body").on("click", "#updateUser", function () {
        // console.log($(this).parents("tr").find("input[name = firstName]").val());
        let firstName = $(this).parents("tr").find("input[name = firstName]").val();
        let lastName = $(this).parents("tr").find("input[name = lastName]").val();
        let mobile = $(this).parents("tr").find("input[name = mobile]").val();
        //then using POST to send them to server
        $.ajax({
            url: './editUser',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                console.log(data);    
            },
            data: JSON.stringify({
                firstname: firstName,
                lastname: lastName, 
                mobile: mobile
            })
        });
        }); 
    
          
        
    
    // when user clicks update to confirm , and returning them to normal state
    $("body").on("click", "#updateUser", function (e) { 
        e.preventDefault();
        
        $(this).parents('tr').find("td:eq(0)").text($(this).parents('tr').find("input[name = firstName]").val());
        $(this).parents('tr').find("td:eq(1)").text($(this).parents('tr').find("input[name = lastName]").val());
        $(this).parents('tr').find("td:eq(2)").text($(this).parents('tr').find("input[name = mobile]").val());
        //displaye the hidden buttons
        $(this).parents('tr').find("#deleteUser").show();
        $(this).parents('tr').find("#editUser").show();
        //remove update and cancel buttons
        $("#updateUser").remove();
        $("#cancelUser").remove();        
    });
    //when user clicks on cancel button
    $("body").on("click", "#cancelUser", function (e) { 
        e.preventDefault();
        $(this).parents("tr").find("td:eq(0)").text($(this).parents("tr").find('td:eq(0)').attr('data-firstname'));
        $(this).parents("tr").find("td:eq(1)").text($(this).parents("tr").find('td:eq(1)').attr('data-lastname'));
        $(this).parents("tr").find("td:eq(2)").text($(this).parents("tr").find('td:eq(2)').attr('data-mobile'));
        $(this).parents('tr').find("#editUser").show();
        $(this).parents('tr').find("#deleteUser").show();
        $("#updateUser").remove();
        $("#cancelUser").remove();        
    });

    //when ADDING A USER
    $("#saveUser").click(function () {
            $.ajax({
                url: './addUser',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    console.log('ADMIN: admin => add user'); 
                },
                data: JSON.stringify({
                    firstname: $("input[name = firstName]").val(),
                    lastname: $("input[name = lastName]").val(),
                    mobile: $("input[name = mobile]").val(),
                })
            });
        
        });


    //when admin clicks on RESET PASSWORD button
    $("body").on("click", "#resetPassword", function () {
        $.ajax({
            url: './userResetPassword',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',    
            success: function (result) {
                console.log('ADMIN: admin => RESET PASSWORD');
                $("#changePass").html("Password of the user: "+result.users.firstName+" has been changed!");
            },
            data: JSON.stringify({
                mobile: $(this).parents("tr").find("td:eq(2)").text(),
            })
        });
    
    }); 

    //when admin clicks on DELETE USER button
    $("body").on("click", "#deleteUser", function () {
        let mobile = $(this).parents("tr").find("td:eq(2)").html();
        $.ajax({
            url: './deleteUser',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                console.log('USER has been DELETED');    
            },
            data: JSON.stringify({
                mobile:mobile
            })
        });
    
        }); 
        
    $("body").on("click", "#deleteUser", function (e) { 
        e.preventDefault();
        $(this).parents('tr').remove();    
        console.log("USER has been DELETED");
        });
});