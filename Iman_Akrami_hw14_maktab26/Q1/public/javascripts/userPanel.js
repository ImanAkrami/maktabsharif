// =======================USER: Functionality==========================
$(document).ready(function(){
    //AJAX to get user info
    $("#showInfoUsers").click(function () {
        $("#tbodyUser").find("tr").remove();
        $.ajax({    
            url: 'user/show',
            type: 'GET',
            success: function (result) {
                $("#tbodyUser").append("<tr id='tr_'></tr>");
                for (let key in result.users) {
                    // console.log(result.users);
                    
                        $("#tr_").append("<td id='tr_"+key+"_"+result.users[key]+"'>"+result.users[key]+"</td>");
                 }                     
                 $('#tr_').append("<td><button class='btn btn-info btn-xs btn-edit' id='editUsers' type='submit'>Edit</button> <button class='btn btn-danger btn-xs btn-delete' id='deleteUsers' type='submit'>Delete</button></td>");
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
   
    //EDIT user button
    $("body").on("click", "#editUsers", function (e) { 
    e.preventDefault();
    var firstName = $(this).parents('tr').find("td:eq(0)").text();
    var lastName = $(this).parents('tr').find("td:eq(1)").text();
    var mobile = $(this).parents('tr').find("td:eq(2)").text();
    var password = $(this).parents('tr').find("td:eq(3)").text();
        

    $(this).parents('tr').find("td:eq(0)").html('<input name="firstName" value="' + firstName + '">');
    $(this).parents('tr').find("td:eq(1)").html('<input name="lastName" value="' + lastName + '">');
    $(this).parents('tr').find("td:eq(2)").html('<input name="mobile" value="' + mobile + '">');
    $(this).parents('tr').find("td:eq(3)").html('<input name="password" value="' + password + '">');
    $(this).parents('tr').find("#deleteUsers").hide();
    $(this).parents("tr").find("td:last").prepend("<button class='btn btn-info btn-xs btn-update' id='updateUsers'>Update</button><button class='btn btn-warning btn-xs btn-cancel' id='cancelUsers'>Cancel</button>");
    $(this).hide();

    });
    //UPDATE/OK button
    $("body").on("click", "#updateUsers", function () {
        // console.log($(this).parents("tr").find("input[name = firstName]").val());
        let firstName=$(this).parents("tr").find("input[name = firstName]").val();
        let lastName=$(this).parents("tr").find("input[name = lastName]").val();
        let mobile=$(this).parents("tr").find("input[name = mobile]").val();
        let password=$(this).parents("tr").find("input[name = password]").val();
        //and then actually sending the post request
        $.ajax({
            url: 'user/editUsers',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                console.log(data);    
            },
            data: JSON.stringify({
                firstname: firstName,
                lastname: lastName,
                mobile: mobile,
                password: password
            })
        });
    
    }); 
    //UPDATE USERS
    $("body").on("click", "#updateUsers", function (e) { 
        e.preventDefault();
        
        $(this).parents('tr').find("td:eq(0)").text($("input[name = firstName]").val());
        $(this).parents('tr').find("td:eq(1)").text($("input[name = lastName]").val());
        $(this).parents('tr').find("td:eq(2)").text($("input[name = mobile]").val());
        $(this).parents('tr').find("td:eq(3)").text($("input[name = password]").val());
        $(this).parents('tr').find("#deleteUsers").show();
        $(this).parents('tr').find("#editUsers").show();

        $("#updateUsers").remove();
        $("#cancelUsers").remove();        
    });

   //CANCEL button
    $("body").on("click", "#cancelUsers", function (e) { 
    e.preventDefault();
    $(this).parents('tr').find("#editUsers").show();
    $(this).parents('tr').find("td:eq(0)").text($("input[name = firstName]").val());
    $(this).parents('tr').find("td:eq(1)").text($("input[name = lastName]").val());
    $(this).parents('tr').find("td:eq(2)").text($("input[name = mobile]").val());
    $(this).parents('tr').find("td:eq(3)").text($("input[name = password]").val());
    $(this).parents('tr').find("#deleteUsers").show();

    $("#updateUsers").remove();
    $("#cancelUsers").remove();        

    });
    //DELETE button
    $("body").on("click", "#deleteUsers" , function () {
        
        let  datauser={mobile:$(this).parents("tr").find("td:eq(2)").text()};
        $(this).parents("tr").remove();
            $.ajax({
                method: "DELETE"
                , url: "user/deleteUser"  
                , data: datauser
                , success: function (data) {
                    console.log(data); 
                } 
            });
            $.ajax({
                method: "GET"
                , url: "user/deleteUser"  
                , data: datauser
                , success: function (data) {
                    console.log(data);
                    
                } 
            });
         
    });


});