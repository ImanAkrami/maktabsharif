//***********THIS IS EXPRESS TEMPLATE*********//
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const chalk = require('chalk');


//Connecting to MongoDB
const mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/mkw14akrami", { useNewUrlParser: true });
console.log(chalk.white.bgGreen.underline.bold('Connected to DB'));

//installation
let installation =  require('./tools/installation');
installation();
console.log(chalk.white.bgGreen.underline.bold('Installation Done'));
//***********THIS IS EXPRESS TEMPLATE*********//
//Routers define
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');
var api = require('./routes/api');

//***********THIS IS EXPRESS TEMPLATE*********//
var app = express();
//Requiring Authentication modules
const session = require('express-session');
const passport = require('passport');

//***********THIS IS EXPRESS TEMPLATE*********//
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Use Session
app.use(session({
  secret: "1!2@3$qQwWeE1qaz2wsx3edc4rfv",
  resave:true,
  cookie:{
    maxAge:600000
  }
}));
app.use(passport.initialize());
app.use(passport.session());

//==================Routers===================
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', api);
app.use('/admin', adminRouter);

//***********THIS IS EXPRESS TEMPLATE*********//
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
