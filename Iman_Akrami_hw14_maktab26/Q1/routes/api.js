var express = require('express');
var router = express.Router();
var app = express();
let User = require('../model/user');
// let generalTools = require('../tools/general-tools');
let ac = require('../tools/ac');
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
const passport = require('passport');
const auth = require('../tools/auth');
const admin = require('./admin');
const user = require('./user');
var path = require('path');
const chalk = require('chalk');

app.use(express.static("public"));

// ============AVATAR UPLOAD==============
//making multer configuration
var storage = multer.diskStorage({
  destination:'./public/uploads',
  
  filename: function (req, file, cb) {
    cb(null,file.fieldname + '-' + Date.now());
  }
});
var upload = multer({ storage: storage }).single('avatar');

// ============ADD ADMIN through PUT==============
// need to use POSTMAN to /api/addAdmin route
router.put('/addAdmin', (req, res) => {
  User.findOne({role: 'admin'}, (err, existAdmin) => {
    if (err) return res.json({success: false, msg: err});
    if (existAdmin) return res.status(404).send('Admin already exists');
    console.log(chalk.red.bgYellow.underline.bold('trying to add Admin'));
    
    new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      password: req.body.password,
      avatar: req.body.avatar,
      role: 'admin',
      mobile: req.body.mobile
    }).save((err, user) => {
      if (err) return res.json({success: false, msg: err});
      console.log(chalk.white.bgGreen.underline.bold('Admin added'));
      return res.json({success: true, user});
    });

  });
});
//===================================================
// ====================LOGIN page====================
// get login page
router.get('/login', function(req, res, next) {
  res.render('login', {msg: null});
});
//===================================================


// ====================AUTHENTICATION > PANEL================
//authentication and BACK to

//OK i still don't know why i can't use authentication to check the GET request #checklater
router.get('/panel',
function(req, res, next) {
  if (req.user.role==='admin') {
    res.render('adminPanel', {
      title: 'admin panel', 
      user: req.user,
      file:null,
      message:null,
    });
  }
});

//  Rendering PANEL according to the logged in user
router.post('/panel', passport.authenticate('local'), async (req, res, next) => {


  if (req.user.role === 'user'){
  console.log(chalk.white.bgGreen.underline.bold('USERLogged in'));
  return res.render('userPanel', 
  {title: 'User: Panel',
  user: req.user
  });
  }
  else 
  {console.log(chalk.white.bgGreen.underline.bold('ADMIN Logged in'));
  return res.render('adminPanel', {
    title: 'Admin: Panel', 
    user: req.user,
    file:null,
    message:null,
    
  });}


});
//=========================================================

// ====================PROFILE PIC UPLOAD================
router.post('/uploads', function (req, res, next) {
  upload(req, res, (err) =>{

    if(err){
      res.render('adminPanel',{
        file:null,
        message : err
      });
    }else{
      if(req.file==undefined){
        res.render('adminPanel',{
          file:null,
          message : "No file has been selected."
        });
      }else{
        res.render('adminPanel',{
          message : "File Uploaded successfully!",
          file:`${req.file.filename}`
        });
        User.findOneAndUpdate({role:"admin"},{
          
          $set: {
              avatar:req.file.filename
          }
          }, (err, result) => {
          console.log(chalk.white.bgGreen.underline.bold('UPLOAD successfull'));
          if (err){
            throw err;
          }
         });
      }
    }

  }); 
});




router.use('/admin', auth.isLogin, ac.routeController(['admin']), admin);
router.use('/user', auth.isLogin, ac.routeController(['user']), user);

module.exports = router;
