//=============requiring the needed modules=============
var express = require("express");
var router = express.Router();
let User = require("../model/user");
// let generalTools = require("../tools/general-tools");
const passport = require("passport");
const auth = require("../tools/auth");
const chalk = require('chalk');

//=============ADMIN: GET ALL USERS=============
router.get("/getAllUsers", (req, res) => {
  res.render("getAllUsers");
  console.log(chalk.white.bgGreen.underline.bold('GET getAllUsers page route'));
});

router.get("/showGetAllUsers", (req, res) => {
  console.log(chalk.white.bgGreen.underline.bold('GET getAllUsers data route'));
  //making a query to db, finding users with role user, removing some unnesseray fields
  User.find(
    { role: "user" },
    { _id: 0, __v: 0, createAt: 0, lastUpdate: 0, password: 0 },
    (err, users) => {
      if (err) return res.json({ success: false, msg: err });
      //returning all of non-admin users in a JSON
      return res.json({ success: true, users });
    }
  );
});

//=============ADMIN: ADD USER=============
//gets data from the post body, and adding as a new user with
// "USER" role
//The function in add user will be ASYNC
router.post("/addUser", async (req, res) => {
  // console.log(req.body);
  let newUser = new User({
    firstName: req.body.firstname,
    lastName: req.body.lastname,
    mobile: req.body.mobile,
    password: req.body.mobile,
    role: "user"
  });
  //and using a usual try/catch , we will AWAIT for the new user to be ready, ( taken from request)
  //and then calling the .save() method.
  let user;
  try {
    user = await newUser.save();
    
  }
  // and of course catching errors , sending ERROR response alongside the msg to be rendered in ejs
  catch (err) {
    return res.json({
      success: false,
      msg: "Unable to save user. chec"
    });
  }
  console.log(chalk.white.bgGreen.underline.bold('ADDED USER THROUGH ADMIN PANEL'));
  //and sending the response ( success) alongside the msg to be displayed through ejs
  return res.json({
    success: true,
    msg: "User has been added.",
    user
  });
  
});


//=============ADMIN: DISPLAY ADMIN INFO ROUTE=============
//get route for getting admin info except id, and a few other fields.
router.get("/editprofile", function(req, res) {
  console.log(chalk.white.bgGreen.underline.bold('GET method for admin info has been called'));
  User.findOne(
    { role: "admin" },
    { _id: 0, __v: 0, createAt: 0, lastUpdate: 0 },
    (err, users) => {
      //returing reulst as JSON.
      return res.json({ success: true, users });
    }
  );
});
//=============ADMIN: INSERT/EDIT ADMIN INFO=============
router.put("/editUserAdmin", (req, res) => {
  console.log(chalk.white.bgGreen.underline.bold('PUT method for edit user admin has been called'));
  //trying to find and update admin user through ROLE field.
  User.findOneAndUpdate(
    { role: "admin" },
    {
      $set: {
        firstName: req.body.firstname,
        lastName: req.body.lastname,
        password: req.body.password,
        role: req.body.role,
        mobile: req.body.mobile
      }
    },
    (err, result) => {
      if (err) {
        throw err;
      }

    }
  );
});
//=============ADMIN: INSERT/EDIT USER INFO=============

router.post("/editUser", (req, res) => {
  console.log(chalk.white.bgGreen.underline.bold('PUT method for edit user admin has been called'));
  console.log(req);
  User.findOneAndUpdate(
    
    
    { role: "user" },
    {
      $set: {
        firstName: req.body.firstname,
        lastName: req.body.lastname,
        mobile: req.body.mobile
      }
    },
    (err, result) => {
      if (err) {
        throw err;
      }

    }
  );
});

//=============ADMIN: PASSWORD RESET FOR USER=============
router.post("/userResetPassword", (req, res) => {
  console.log(chalk.white.bgGreen.underline.bold('PUT method for edit user admin has been called'));

  User.findOneAndUpdate(
    { mobile: req.body.mobile },
    {
      $set: {
        password: req.body.mobile
      }
    },
    (err, users) => {
      res.json({ success: true, users });
    }
  );
  console.log("change password user!!!!");
});

//=============ADMIN: USER DELETE=============
router.post("/deleteUser", (req, res) => {
  console.log(chalk.white.bgGreen.underline.bold('USER has been deleted through POST'));
  User.deleteOne({ mobile: req.body.mobile }, function(err) {
    console.log(err);
  });
});

module.exports = router;
