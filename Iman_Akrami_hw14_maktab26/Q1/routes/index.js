var express = require('express');
var router = express.Router();
const chalk = require('chalk');

// ====================GET HOME PAGE================
router.get('/', function(req, res, next) {
  console.log(chalk.white.bgGreen.underline.bold('GET ROOT and then LOGIN'));
  res.render('login', { title: 'Express' });
});
// ====================LOGOUT ROUTE================
router.get('/logout', function(req, res) {
  req.logout();
  console.log(chalk.white.bgGreen.underline.bold('LOGOUT'));
  res.redirect('./api/login');
    
});

module.exports = router;
