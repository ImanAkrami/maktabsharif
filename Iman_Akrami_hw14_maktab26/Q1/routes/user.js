var express = require('express');
var router = express.Router();
let User = require('../model/user');
const passport = require('passport');
const auth = require('../tools/auth');
const chalk = require('chalk');

// ====================USER route for GET request================
router.get('/show', (req, res) => {
    console.log(chalk.white.bgGreen.underline.bold('GET request made to USER SHOW route'));
    User.findOne({role:"user",mobile:req.user.mobile},{_id:0,__v:0,createAt:0,lastUpdate:0}, (err, users) => {
  
        if (err) return res.json({success: false, msg: 'Unable to GET data'});
        
        return res.json({success: true, users});
      });
});

// ====================USER route for POST request to EDIT================
router.post('/editUsers', (req, res) => {
    console.log(chalk.white.bgGreen.underline.bold('POST request made to EDITUSER route'));    
    User.findOneAndUpdate({role:"user"},{
        $set: {
            firstName : req.body.firstname,
            lastName : req.body.lastname,
            mobile : req.body.mobile,
            password : req.body.password
        }
    }, (err, result) => {
        console.log(err);
    });
  });
// ====================USER route for DELETE request to DELETE================
  router.delete('/deleteUser', (req, res) => {
    console.log(chalk.white.bgGreen.underline.bold('DELETE request made to DELETE route')); 
    User.deleteOne({mobile:req.body.mobile}, (err, result) => {
            res.json({msg: "User Deleted", result});
    });
});


  
module.exports = router;
