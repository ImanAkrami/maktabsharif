console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.6 starts from here:
// ======================================

let sampleNestedArray = [10, [25, 13], [14, [55]], 2];

function flatten(array) {
  return array.reduce((acc, e) => {
    if (Array.isArray(e)) {
      return acc.concat(flatten(e));
    } else {
      return acc.concat(e);
    }
  }, []);
}

console.log(flatten([1, 2, 3, [4, 5, [6, 7, [9, 8]]]]));
