console.log('Connected to the browser. ready to go!');




// ======================================
// The answer for Q2.1 starts from here:
// ======================================

// st is used with numbers ending in 1 (e.g. 1st, pronounced first)
// nd is used with numbers ending in 2 (e.g. 92nd, pronounced ninety-second)
// rd is used with numbers ending in 3 (e.g. 33rd, pronounced thirty-third)
// As an exception to the above rules, all the "teen" numbers ending with 11, 12 or 13 use -th (e.g. 11th, pronounced eleventh, 112th, pronounced one hundred [and] twelfth)
// th is used for all other numbers (e.g. 9th, pronounced ninth).
let o = ['rd', 'th', 'nd', 'st'];
let color = ['Blue', 'Green', 'Red', 'Black', 'Yellow', 'Orange', 'White',
'Purple', 'Violet', 'Indigo', 'Gray', 'Pink'];
// So in total, the following function will work on ANY number.

function ordinalSuffixAdder(InputArray,SuffixArray) {
   
    for (let index = 0; index < InputArray.length; index++) {
    let j = (index+1)% 10, k = (index+1)% 100;
    if (j == 1 && k != 11) {
        console.log(InputArray[index]+ " is the "+ (index+1)+ SuffixArray[3]+ " color.");
    }
    else if (j == 2 && k != 12) {
        console.log(InputArray[index]+ " is the "+(index+1)+ SuffixArray[2]+ " color.");
    }
    else if (j == 3 && k != 13) {
        console.log(InputArray[index]+ " is the "+(index+1)+ SuffixArray[0]+ " color.");
    }
    else{
        console.log(InputArray[index]+ " is the "+(index+1)+ SuffixArray[1]+ " color.");
    }
        
    }

    
}

console.log(ordinalSuffixAdder(color,o))