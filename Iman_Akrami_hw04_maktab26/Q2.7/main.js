console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.7 starts from here:
// ======================================

var personData = {
  person1: {
    uid: 112233,
    city: "isfahan",
    postalCode: 2345672345,
    phoneNumber: "03111234234",
    position: "ui designer"
  },
  person2: {
    uid: 223344,
    city: "abhar",
    postalCode: 3345673232,
    phoneNumber: "04111334452",
    position: "analyzer"
  },
  person3: {
    uid: 334455,
    city: "rasht",
    ostalCode: 9945643232,
    phoneNumber: "01131394855",
    position: "ui designer"
  },
  person4: {
    uid: 445566,
    city: "mashad",
    postalCode: 5545689232,
    phoneNumber: "04121334415",
    position: "ui designer"
  },
  peson5: {
    uid: 556677,
    city: "semnan",
    postalCode: 774565392,
    phoneNumber: "09331334225",
    position: "analyzer"
  },
  person6: {
    uid: 667788,
    city: "shiraz",
    postalCode: 7845482232,
    phoneNumber: "07771333455",
    position: "php programmer"
  },
  person7: {
    uid: 778899,
    city: "zahedan",
    postalCode: 1145119212,
    phoneNumber: "01221399450",
    position: "ux designer"
  },
  person8: {
    uid: 889900,
    city: "qom",
    postalCode: 8845383233,
    phoneNumber: "08121320452",
    position: "node programmer"
  },
  person9: {
    uid: 990011,
    city: "ahvaz",
    postalCode: 2242689035,
    phoneNumber: "02211783452",
    position: "ux designer"
  },
  person10: {
    uid: 113344,
    city: "arak",
    postalCode: 1145129244,
    phoneNumber: "01221334665",
    position: "java programmer"
  }
};

var additionalPersonData = {
  person11: {
    uid: 223344,
    firstName: "amirhosein",
    lastName: "kazemi"
  },
  person12: {
    uid: 112233,
    firstName: "reza",
    lastName: "hosseini"
  },
  person13: {
    uid: 334455,
    firstName: "soheil",
    lastName: "hosein"
  },
  person14: {
    uid: 445566,
    firstName: "shahriar",
    lastName: "ahmadi gol"
  },
  person15: {
    uid: 556677,
    firstName: "ahamad",
    lastName: "rezai"
  },
  person16: {
    uid: 667788,
    firstName: "mohammadhadi",
    lastName: "soleimani"
  },
  person17: {
    uid: 778899,
    firstName: "mohsen",
    lastName: "zare"
  },
  person18: {
    uid: 889900,
    firstName: "mahdi",
    lastName: "mohseni nasab"
  },
  person19: {
    uid: 990011,
    firstName: "milad",
    lastName: "rabbani"
  },
  person20: {
    uid: 113344,
    firstName: "ali",
    lastName: "ahmadi"
  }
};
//ُSolution 1
//Using For in loop and assign

function personDataMerger(userInfo1stSegment, userInfo2ndSegment, prop) {
  for (const key1 in userInfo1stSegment) {
    for (const key2 in userInfo2ndSegment) {
      if (userInfo1stSegment[key1][prop] === userInfo2ndSegment[key2][prop]) {
        Object.assign(userInfo1stSegment[key1], userInfo2ndSegment[key2]);
      }
    }
  }
  return userInfo1stSegment;
}
console.log(personDataMerger(personData,additionalPersonData,'uid'));
//ُSolution 2
//using Spread method
function personDataMerger(userInfo1stSegment, userInfo2ndSegment, prop) {
  for (const key1 in userInfo1stSegment) {
    for (const key2 in userInfo2ndSegment) {
      if (userInfo1stSegment[key1][prop] === userInfo2ndSegment[key2][prop]) {
        userInfo1stSegment[key1] = {...userInfo1stSegment[key1],...userInfo2ndSegment[key2]}
      }
    }
  }
  return userInfo1stSegment;
}
console.log(personDataMerger(personData,additionalPersonData,'uid'));
