console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.3 starts from here:
// ======================================
let testImpureArray = [null,2,undefined,undefined,0,NaN,false,true,"",1,0,3]

function arrayEmptyValueRemover(inputImpureArray) {
  let trashableItems = [null, 0, undefined, "", NaN, false];
  for (let i = 0; i < inputImpureArray.length; i++) {
    for (let j = 0; j < trashableItems.length; j++) {
        if (inputImpureArray[i]==trashableItems[j] || inputImpureArray[i]!==inputImpureArray[i])
        // for NaN values , i could also use Number.isNaN(inputImpureArray[i]) , but i did'nt
        // the reason is that it's not supported except in new browsers. instead, i took advantage
        // of a quirk that only NaN has: it's the only Javascript Variable that is not equal to itself.
        //therefore, i checked for it and if it's true, the codes returns true, and removes the NaN value.
        {
            inputImpureArray.splice(i,1)
        }
        
    }
  }
  return inputImpureArray;
}

console.log(arrayEmptyValueRemover(testImpureArray));
