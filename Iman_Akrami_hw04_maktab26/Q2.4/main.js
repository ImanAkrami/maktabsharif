console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.4 starts from here:
// ======================================

//i will show two ways to accomplish this:
//============================
//First method, using Array constructor and fill method
function numberOfNumberGeneratorFirstVersion(howMany,theDigit) {
  return Array(howMany).fill(theDigit)
}
//testing
console.log(numberOfNumberGeneratorFirstVersion(10,2));
//============================
//Second method, using a loop

function numberOfNumberGeneratorSecondVersion(lenght,desiredDigit) {
  let outPutArray = [];
  for (let i = 0; i < lenght; i++) {
    outPutArray.push(desiredDigit);
  }
  return outPutArray;
}
//testing
console.log(numberOfNumberGeneratorSecondVersion(5,1));

//============================
//Third method using joni/split and then return the desired
//number on each element using map:
function numberOfNumberGeneratorThirdVersion(desiredLength,theRepeatingDigit) {
  return Array(desiredLength).join(1).split('').map(function(){return theRepeatingDigit;})  
}

//testing
console.log(numberOfNumberGeneratorThirdVersion(15,3));
