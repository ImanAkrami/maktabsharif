console.log("Connected to the browser. ready to go!");

// ======================================
// The answer for Q2.5 starts from here:
// ======================================

//defining a sample text variable
let sampleText = "I really LOVE learning javascript at MAKTAB";
//==========
//First solution, compatible with older browsers
//==========
function caseSwapper1stVersion(inputString) {
  return inputString.split('').map(function(c){
    return c === c.toUpperCase()
    ? c.toLowerCase()
    : c.toUpperCase()
  }).join('')
}

//testing
console.log(caseSwapper1stVersion(sampleText))
//==========
//Second solution, compatible with ES6
//==========
function caseSwapper2ndVersion(stringToSwapCase) {
  return stringToSwapCase
    .split("")
    .map(charachter => (charachter === charachter.toUpperCase() ? charachter.toLowerCase() : charachter.toUpperCase()))
    .join("");
}
//testing
console.log(caseSwapper2ndVersion(sampleText));

//==========
//Third solution, using a while loop and string methods
//==========

function caseSwapper3rdVersion (stringToBeSwapped) {
  let finalSwappedString = '';
  let stringCharachterIndex = 0;
  while (stringCharachterIndex < stringToBeSwapped.length) {
      var CurrentCharachter = stringToBeSwapped.charAt(stringCharachterIndex);
      if (CurrentCharachter == CurrentCharachter.toUpperCase()) {
          // convert toLowerCase
          CurrentCharachter = CurrentCharachter.toLowerCase();
      } else {
          // convert toUpperCase
          CurrentCharachter = CurrentCharachter.toUpperCase();
      }

      stringCharachterIndex += 1;
      finalSwappedString += CurrentCharachter; 
  }
  return finalSwappedString;
};
//testing
console.log(caseSwapper3rdVersion(sampleText));