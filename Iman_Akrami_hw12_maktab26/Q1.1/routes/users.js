const express = require('express');
const router = express.Router();
//for encrypting password:
const bcrypt = require('bcryptjs');
const passport = require('passport');
// Load User model
const User = require('../models/User');
const { forwardAuthenticated } = require('../config/auth');

// Login Page
router.get('/login', forwardAuthenticated, (req, res) => res.render('login'));

// Register Page
router.get('/register', forwardAuthenticated, (req, res) => res.render('register'));

// Register
router.post('/register', (req, res) => {
  const { name, email, password, password2 } = req.body;
  let errors = [];

  if (!name || !email || !password || !password2) {
    errors.push({ msg: 'Please enter all fields' });
  }

  if (password != password2) {
    errors.push({ msg: 'Passwords do not match' });
  }

  if (password.length < 6) {
    errors.push({ msg: 'Password must be at least 6 characters' });
  }

  if (errors.length > 0) {
    res.render('register', {
      errors,
      name,
      email,
      password,
      password2
    });
  } else {
      //validtion passed

      //checking if user exist?
    User.findOne({ email: email }).then(user => {
        //so the user exist, we will rerender the register form
      if (user) {
          //pushing an error to errors array
        errors.push({ msg: 'Email already exists' });
        //rerending the register page, while passing the variables
        res.render('register', {
          errors,
          name,
          email,
          password,
          password2
        });
      } else {
          //okay, there is not an existing user? we will add a new one then. 
          // but we have to encrypt the password first bcrypt will do that for us
        const newUser = new User({
          name,
          email,
          password
        });
        // generating a SALT so we an can generate a hash
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            //checking for error
            if (err) throw err;
            //setting the password to the hash of it
            newUser.password = hash;
            // now SAVING user to the Database
            newUser
              .save()
              .then(user => {
                req.flash(
                  'success_msg',
                  'You are now registered and can log in'
                );
                res.redirect('/users/login');
              })
              .catch(err => console.log(err));
          });
        });
      }
    });
  }
});

// Login
router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next);
});

// Logout
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/users/login');
});

module.exports = router;